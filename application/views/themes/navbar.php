<!-- NAVIGATION STARTS
			========================================================================= -->
		<nav id="navigation" class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html"><img src="<?= base_url() ?>assets/frontend/images/logo-menu.png" height="70" alt="" ></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						
						<li><a href="<?= base_url() ?>" class="external">Home</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url()?>contact" class="external">Contact us</a></li>
								<li><a href="<?= base_url()?>about-institute" class="external">About Institute</a></li>
								<li><a href="<?= base_url()?>guruji-sanjeev" class="external">Guruji Sanjeev</a></li>
								<li><a href="<?= base_url()?>centres" class="external">Our Centres</a></li>
							</ul>
						</li>
						<li class="dropdown <?php if($this->uri->segment(1) == 'programmes'){ echo 'active'; }?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programs <span class="caret"></span></a>
							<ul class="dropdown-menu">
							<?php foreach($programmes as $programme) :?>
								<li><a href="<?= base_url().'programmes/'.$programme->slug?>" class="external"><?= $programme->title?></a></li>
							<?php endforeach;?>
							</ul>
						</li>
						<li class="<?php if($this->uri->segment(1) == 'trainers'){ echo 'active'; }?>"><a href="<?= base_url()?>trainers" class="external">Trainers</a></li>
						<li><a href="<?= base_url()?>news-and-events" class="external">News & Events</a></li>
						<li class="dropdown">
							<a href="<?= base_url()?>testimonials" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gallery <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url()?>student-experiences" class="external">Image Gallery</a></li>
								<li><a href="<?= base_url()?>video-gallery" class="external">Video Gallery</a></li>
							</ul>
						</li>

						<!-- <li><a href="<?= base_url()?>stories" class="external">Stories</a></li> -->
						<li class="<?php if($this->uri->segment(1) == 'blogs'){ echo 'active'; }?>"><a href="<?= base_url()?>blogs" class="external">Blog</a></li>
						<!-- <li><a a href="" class="white" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li> -->
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
