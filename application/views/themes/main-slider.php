<div id="rev_slider_46_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="notgeneric1" style="background-color:transparent;padding:0px;">
				<!-- START REVOLUTION SLIDER 5.0.7 fullscreen mode -->
				<div id="rev_slider_46_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.0.7">
					<ul>
						<!-- SLIDE  -->
						<li data-index="rs-148" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?= base_url() ?>assets/frontend/images/slider/slide-1.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Rhythm Yoga" data-description="">
							<!-- MAIN IMAGE -->							
							<img src="<?= base_url() ?>assets/frontend/images/slider/slide-1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-1" 
								data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','15']" 
								data-fontsize="['30','30','30','18']"
								data-lineheight="['30','30','30','18']"
								data-width="none"
								data-height="none"
								data-color="#FFF"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1000" 
								data-splitin="chars" 
								data-splitout="none" 
								data-responsive_offset="on" 
								data-elementdelay="0.05" 
								style="z-index: 5; white-space: nowrap">a new way, a new outlook, a new transcendence, a new beginning.
 
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-2" 
								data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-15']" 
								data-fontsize="['50','50','50','30']"
								data-lineheight="['50','50','50','30']"
								data-width="none"
								data-height="none"
								data-color="#FFF"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1500" 
								data-splitin="none" 
								data-splitout="none" 
								data-responsive_offset="on" 
								style="z-index: 6; white-space: nowrap;">A new dawn, a new era 
							</div>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-149" data-transition="fadein" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?= base_url() ?>assets/frontend/images/slider/slide-2.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Wisdom" data-description="">
							<!-- MAIN IMAGE -->
							<img src="<?= base_url() ?>assets/frontend/images/slider/slide-2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" 
								id="slide-149-layer-1" 
								data-x="['right','right','center','center']" data-hoffset="['0','0','50','50']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','15']" 
								data-fontsize="['30','30','30','18']"
								data-lineheight="['30','30','30','18']"
								data-width="none"
								data-height="none"
								data-color="#000"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1000" 
								data-splitin="chars" 
								data-splitout="none" 
								data-responsive_offset="on" 
								data-elementdelay="0.05" 
								style="z-index: 5; white-space: nowrap;">the human himself is the book, and He himself is the university
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
								id="slide-149-layer-2" 
								data-x="['right','right','center','center']" data-hoffset="['0','0','50','50']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['-40','-40','-40','-15']" 
								data-fontsize="['50','50','50','26']"
								data-lineheight="['50','50','50','26']"
								data-width="none"
								data-height="none"
								data-color="#000"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_out="x:inherit;y:inherit;" 
								data-start="1500" 
								data-splitin="none" 
								data-splitout="none" 
								data-responsive_offset="on" 
								style="z-index: 6; white-space: nowrap;">A life of books has to end
							</div>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-150" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?= base_url() ?>assets/frontend/images/slider/slide-3.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Universal" data-description="">
							<!-- MAIN IMAGE -->							
							<img src="<?= base_url() ?>assets/frontend/images/slider/slide-3.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-1" 
								data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','15']" 
								data-fontsize="['30','30','30','18']"
								data-lineheight="['30','30','30','18']"
								data-width="none"
								data-height="none"
								data-color="#FFF"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1000" 
								data-splitin="chars" 
								data-splitout="none" 
								data-responsive_offset="on" 
								data-elementdelay="0.05" 
								style="z-index: 5; white-space: nowrap">a language beyond all languages, a language which every soul in the world can understand
 
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-2" 
								data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-15']" 
								data-fontsize="['50','50','50','30']"
								data-lineheight="['50','50','50','30']"
								data-width="none"
								data-height="none"
								data-color="#FFF"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1500" 
								data-splitin="none" 
								data-splitout="none" 
								data-responsive_offset="on" 
								style="z-index: 6; white-space: nowrap;">A language universal 
							</div>
						</li>
						<!-- SLIDE  -->
						<li data-index="rs-151" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?= base_url() ?>assets/frontend/images/slider/slide-4.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Knowledge" data-description="">
							<!-- MAIN IMAGE -->							
							<img src="<?= base_url() ?>assets/frontend/images/slider/slide-4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-1" 
								data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['-20','-20','-20','-15']" 
								data-fontsize="['30','30','30','18']"
								data-lineheight="['30','30','30','18']"
								data-width="none"
								data-height="none"
								data-color="#000"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1000" 
								data-splitin="chars" 
								data-splitout="none" 
								data-responsive_offset="on" 
								data-elementdelay="0.05" 
								style="z-index: 5; white-space: nowrap">A life in humans as universal ,scientific and logical brain in  knowledge
 
							</div>
							
						</li>

						<!-- SLIDE  -->
						<li data-index="rs-152" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?= base_url() ?>assets/frontend/images/slider/slide-5.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Kalari" data-description="">
							<!-- MAIN IMAGE -->							
							<img src="<?= base_url() ?>assets/frontend/images/slider/slide-5.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
							<!-- LAYERS -->
							<!-- LAYER NR. 1 -->
							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-1" 
								data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','15']" 
								data-fontsize="['30','30','30','18']"
								data-lineheight="['30','30','30','18']"
								data-width="none"
								data-height="none"
								data-color="#000"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1000" 
								data-splitin="chars" 
								data-splitout="none" 
								data-responsive_offset="on" 
								data-elementdelay="0.05" 
								style="z-index: 5; white-space: nowrap">Mother of all self-defense – a knowledge for a dynamic/vibrant life.
							</div>
							
						</li>

						<!-- SLIDE  -->
						<li data-index="rs-153" data-transition="zoomin" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="<?= base_url() ?>assets/frontend/images/slider/slide-6.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Effo1rt" data-description="">
							<!-- MAIN IMAGE -->							
							<img src="<?= base_url() ?>assets/frontend/images/slider/slide-6.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
							<!-- LAYER NR. 1 -->
							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-1" 
								data-x="['center','center','center','center']" data-hoffset="['90','90','90','90']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['20','20','20','15']" 
								data-fontsize="['30','30','30','18']"
								data-lineheight="['30','30','30','18']"
								data-width="none"
								data-height="none"
								data-color="#FFF"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1000" 
								data-splitin="chars" 
								data-splitout="none" 
								data-responsive_offset="on" 
								data-elementdelay="0.05" 
								style="z-index: 5; white-space: nowrap">A scientific approach for clarity at all levels
 
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
								id="slide-148-layer-2" 
								data-x="['center','center','center','center']" data-hoffset="['90','90','90','90']" 
								data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-15']" 
								data-fontsize="['50','50','50','30']"
								data-lineheight="['50','50','50','30']"
								data-width="none"
								data-height="none"
								data-color="#000"
								data-whitespace="nowrap"
								data-transform_idle="o:1;"
								data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								data-start="1500" 
								data-splitin="none" 
								data-splitout="none" 
								data-responsive_offset="on" 
								style="z-index: 6; white-space: nowrap;">Yoga in Psychotherapy
							</div>
							
						</li>


					
					</ul>
					<div class="tp-bannertimer tp-top" style="visibility: hidden !important;"></div>
				</div>
			</div>
			<!-- END REVOLUTION SLIDER -->