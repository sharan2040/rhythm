<!DOCTYPE html>
<html class="bootstrap-layout">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Dashboard | Rhythm Yoga</title>

  <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
  <meta name="robots" content="noindex">

  <script src="<?=base_url()?>assets/admin/assets/vendor/jquery.min.js"></script>

  <!-- Material Design Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Roboto Web Font -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">

  <!-- App CSS -->
  <link type="text/css" href="<?=base_url()?>assets/admin/assets/css/style.min.css" rel="stylesheet">
  <link type="text/css" href="<?=base_url()?>assets/admin/assets/css/custom.css" rel="stylesheet">

  <!-- Charts CSS -->
  <link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/morris.min.css">

  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="icon" type="image/png" href="<?=base_url()?>assets/favicon.png" />

  

</head>

<body class="layout-container ls-top-navbar si-l3-md-up">

  <!-- Navbar -->
  <nav class="navbar navbar-light bg-white navbar-full navbar-fixed-top ls-left-sidebar">

    <!-- Sidebar toggle -->
    <button class="navbar-toggler pull-xs-left hidden-lg-up" type="button" data-toggle="sidebar" data-target="#sidebarLeft"><span class="material-icons">menu</span></button>

    <!-- Brand -->
    <a class="navbar-brand first-child-md" href="<?=base_url()?>admin/dashboard">Dashboard</a>

    <!-- Search -->
    <form class="form-inline pull-xs-left hidden-sm-down">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn"><button class="btn" type="button"><i class="material-icons">search</i></button></span>
      </div>
    </form>
    <!-- // END Search -->

    <!-- Menu -->
    <ul class="nav navbar-nav pull-xs-right hidden-md-down">

       <!-- Notifications dropdown -->
<!--       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-caret="false" data-toggle="dropdown" role="button" aria-haspopup="false"><i class="material-icons email">mail_outline</i></a>
        <ul class="dropdown-menu dropdown-menu-right notifications" aria-labelledby="Preview">
          <li class="dropdown-title">Emails</li>
          <li class="dropdown-item email-item">
            <a class="nav-link" href="index.html">
              <span class="media">
					<span class="media-left media-middle"><i class="material-icons">mail</i></span>
              <span class="media-body media-middle">
						<small class="pull-xs-right text-muted">12:20</small>
						<strong>Guruji Sanjeev</strong>
						Enhance your website with
					</span>
              </span>
            </a>
          </li>
          <li class="dropdown-item email-item">
            <a class="nav-link" href="index.html">
              <span class="media">
					<span class="media-left media-middle">
						<i class="material-icons">mail</i>
					</span>
              <span class="media-body media-middle">
						<small class="text-muted pull-xs-right">30 min</small>
						<strong>Michael Brain</strong>
						Partnership proposal
					</span>
              </span>
            </a>
          </li>
          <li class="dropdown-item email-item">
            <a class="nav-link" href="index.html">
              <span class="media">
					<span class="media-left media-middle">
						<i class="material-icons">mail</i>
					</span>
              <span class="media-body media-middle">
						<small class="text-muted pull-xs-right">1 hr</small>
						<strong>Sammie Downey</strong>
						UI Design
					</span>
              </span>
            </a>
          </li>
          <li class="dropdown-action center">
            <a href="email.html">Go to inbox</a>
          </li>
        </ul>
      </li> -->
      <!-- // END Notifications dropdown --> 

      <!-- User dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link active dropdown-toggle p-a-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false">
          <img src="<?=base_url()?>assets/admin/assets/guruji.jpg" alt="Avatar" class="img-circle" width="40">
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-list" aria-labelledby="Preview">
<!--           <a class="dropdown-item" href="#"><i class="material-icons md-18">lock</i> <span class="icon-text">Edit Account</span></a>
          <a class="dropdown-item" href="#"><i class="material-icons md-18">person</i> <span class="icon-text">Public Profile</span></a> -->
          <a class="dropdown-item" href="<?=base_url()?>admin/logout">Logout</a>
        </div>
      </li>
      <!-- // END User dropdown -->

    </ul>
    <!-- // END Menu -->

  </nav>
  <!-- // END Navbar -->

  <!-- Sidebar -->
  <div class="sidebar sidebar-left si-si-3 sidebar-visible-md-up sidebar-dark bg-primary" id="sidebarLeft" data-scrollable>

    <!-- Brand -->
    <a href="<?=base_url()?>" class="sidebar-brand">
      <!-- <img src="<?=base_url()?>assets/admin/assets/images/logo.png" height="40" > -->
      <img src="<?=base_url()?>assets/admin/assets/images/logo.png" height="23" > Rhythm Yoga
    </a>

    <!-- User -->
    <a href="user-profile.html" class="sidebar-link sidebar-user">
      <img src="<?=base_url()?>assets/admin/assets/guruji.jpg" alt="user" class="img-circle"> Guruji Sanjeev
    </a>
    <!-- // END User -->

    <!-- Menu -->
    <ul class="sidebar-menu sm-bordered sm-active-button-bg">
    
      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/dashboard">
          <i class="sidebar-menu-icon material-icons">home</i> Dashboard
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/programmes">
          <i class="sidebar-menu-icon material-icons">event_note</i> Main Programmes
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/trainers">
          <i class="sidebar-menu-icon material-icons">supervisor_account</i> Trainers
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/centres">
          <i class="sidebar-menu-icon material-icons">account_balance</i> Centres
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/blogs">
          <i class="sidebar-menu-icon material-icons">style</i> Blogs
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/reminders">
          <i class="sidebar-menu-icon material-icons">notifications</i> Reminders
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/NewsandEvents">
          <i class="sidebar-menu-icon material-icons">event</i> News & Events
        </a>
      </li>

<!--       <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/events">
          <i class="sidebar-menu-icon material-icons">collections_bookmark</i> Events
        </a>
      </li>
 -->
      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/testimonials">
          <i class="sidebar-menu-icon material-icons">chat</i> Short Testimonials
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/Experiences">
          <i class="sidebar-menu-icon material-icons">subject</i> Stories
        </a>
      </li>

      <li class="sidebar-menu-item">
        <a class="sidebar-menu-button" href="<?=base_url()?>admin/settings">
          <i class="sidebar-menu-icon material-icons">settings_applications</i> Settings
        </a>
      </li>

    </ul>

    <!-- // END Menu -->

    <!-- Activity -->
    <div class="sidebar-heading">Reminders for Today</div>
    <?php if(!empty($reminders)) :?>
    <ul class="sidebar-activity">
     
     <?php foreach($reminders as $reminder): ?>
      <li class="media">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">notifications_active</i>
          </div>
        </div>
        <div class="media-body">
          <?= $reminder->reminder?>
          <small>Added on <?= date('d M Y', strtotime($reminder->reminder_date))?></small>
        </div>
      </li>
    <?php endforeach;?>

    </ul>
  <?php else :?>
    <ul class="sidebar-activity">
    <li class="media">
        <div class="media-left">
          <div class="sidebar-activity-icon">
            <i class="material-icons">sentiment_neutral</i>
          </div>
        </div>
        <div class="media-body">
          Hmm. I got nothing to remind you today
        </div>
      </li>
      </ul>
  <?php endif;?>
    <!-- // END Activity -->

    <!-- Stats -->
<!--     <div class="sidebar-stats">
      <div class="sidebar-stats-lead text-primary">
        <span>410</span>
        <small class="text-success">
          <i class="material-icons md-middle">arrow_upward</i>
          <span class="icon-text">3.4%</span>
        </small>
      </div>
      <small>TOTAL ORDERS</small>
    </div>
    <!-- // END Stats -->

  </div> -->
  <!-- // END Sidebar -->

  <!-- Right Sidebars -->

 