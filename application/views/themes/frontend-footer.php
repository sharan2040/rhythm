		<!-- STRIP STARTS
			========================================================================= -->
		<div class="totop-strip blue-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 footer-line">First ISO Certified Yoga Centre in the Middle East | Working Hours : <?= $settings[0]->working_hours; ?></div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
						<div class="scrollup"><a href="#"></a></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /. STRIP ENDS
			========================================================================= -->
		<!-- FOOTER STARTS
			========================================================================= -->
		<footer class="dark-grey-bg">
			<div class="container">
				<div class="row">
					<!-- Stories Starts -->
					<div class="col-lg-4 clearfix f-stories">
						<div class="stories-carousel">
							<!-- Story Starts -->
							<div class="block">
								<h1>Testimonials</h1>
								<div class="picture">
									<img src="<?= base_url() ?>assets/frontend/images/news/carousel/1.jpg" class="img-responsive" alt="" >
									<div class="name">Eric<br>Marcovich</div>
								</div>
								<div class="description">Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. </div>
							</div>
							<!-- Story Ends -->
							<!-- Story Starts -->
							<div class="block">
								<h1>Testimonials</h1>
								<div class="picture">
									<img src="<?= base_url() ?>assets/frontend/images/news/carousel/1.jpg" class="img-responsive" alt="" >
									<div class="name">Eric<br>Marcovich</div>
								</div>
								<div class="description">Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. </div>
							</div>
							<!-- Story Ends -->
						</div>
					</div>
					<!-- Stories Ends -->
					<!-- Links Starts -->
					<div class="col-lg-5 links">
						<div class="block">
							<h1>Pages</h1>
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
									<ul class="links">
										<li><a href="<?= base_url() ?>">Home</a></li>
										<li><a href="<?= base_url()?>news-and-events">News & Events</a></li>
										<li><a href="<?= base_url()?>blogs">Blogs</a></li>
										<li><a href="">Images</a></li>
										<li><a href="">Videos</a></li>
									</ul>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
									<ul class="links">
										<li><a href="">Guruji Sanjeev</a></li>
										<li><a href="<?= base_url()?>about-institute">About Institute</a></li>
										<li><a href="<?= base_url()?>contact">Contact Us</a></li>
										<li><a href="<?= base_url()?>trainers" class="external">Trainers</a></li>
										<li><a href="<?= base_url()?>centres" class="external">Centres</a></li>
									</ul>
								</div>
								<div class="col-lg-5 instagram">
									<!-- SnapWidget -->
									<script src="http://snapwidget.com/js/snapwidget.js"></script>
									<iframe src="http://snapwidget.com/in/?h=ZW52YXRvc3R1ZGlvfGlufDQwfDJ8Mnx8bm98OHxmYWRlT3V0fG9uU3RhcnR8eWVzfHllcw==&amp;ve=121115" title="Instagram Widget" class="snapwidget-widget" style="border:none; overflow:hidden; width:100%;"></iframe>
									Instagram Photos
								</div>
							</div>
						</div>
					</div>
					<!-- Links Ends -->
					<!-- Contact Starts -->
					<div class="col-lg-3 contact">
						<div class="block">
							<h1>Contact us</h1>
							<ul>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="text">Telephone:  <?= $settings[0]->contact_phone; ?></div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-fax"></i></div>
									<div class="text">Fax/phone:  <?= $settings[0]->contact_fax; ?></div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<div class="text">E-mail:  <a href=""><?= $settings[0]->contact_email; ?></a></div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-map-marker"></i></div>
									<div class="text">Adress: <?= $settings[0]->company_address; ?></div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Contact Ends -->
				</div>
			</div>
		</footer>
		<!-- /. FOOTER ENDS
			========================================================================= -->
		<!-- COPYRIGHT STARTS
			========================================================================= -->
		<div class="copyright medium-grey-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">Copyright © <?= date('Y')?> Rhythm Yoga | All Rights Reserved | <a href="">Terms &amp; Conditions</a></div>
					<div class="col-lg-4">
						<ul class="social-icons">
							<li>Social Links:</li>
							<li><a href=""><i class="fa fa-facebook"></i></a></li>
							<li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href=""><i class="fa fa-instagram"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /. COPYRIGHT ENDS
			========================================================================= -->

			<!-- Modal -->
			<div id="loginModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content modal-frontend">
						<form role="form" action="<?php echo base_url()?>staticpages/submitTestimonial" method="POST">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" align="center"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Login</h4>
							</div>
							<div class="modal-body">

								<div class="form-group">
									<label for="email">Email Address</label>
									<input type="text" class="form-control" id="email" name="name">
								</div>

								<div class="form-group">
									<label for="email">Password</label>
									<input type="email" class="form-control" id="email" name="email">
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-success"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								</div>


								
								
							</div>
							<div class="modal-footer">
								<p>Don't have an account ? <a>Sign up</a></p>
							</div>
						</form>
					</div>

				</div>
			</div>
		
		<a href="https://www.comodo.com/" target="_blank"><img class="comodo hidden-xs" src= "<?= base_url();?>assets/comodo.png"/></a>

		
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?= base_url() ?>assets/frontend/js/bootstrap.min.js"></script>
		<!-- Style Switcher -->
		<script src="<?= base_url() ?>assets/frontend/js/styleswitcher/styleswitcher.js" type="text/javascript"></script>
		<script>
			$(document).ready(function() {
			    "use strict";
			    $("#hide, #show").click(function() {
			        if ($("#show").is(":visible")) {
			
			            $("#show").animate({
			                "margin-left": "-400px"
			            }, 400, function() {
			                $(this).hide();
			            });
			
			            $("#switch").animate({
			                "margin-left": "0px"
			            }, 400).show();
			        } else {
			            $("#switch").animate({
			                "margin-left": "-400px"
			            }, 400, function() {
			                $(this).hide();
			            });
			            $("#show").show().animate({
			                "margin-left": "0"
			            }, 400);
			        }
			    });			
			});					
		</script>
		<!-- Isotope Gallery --> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/isotope/jquery.isotope.min.js"></script> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/isotope/custom-isotope-mansory.js"></script>
		<!-- Magnific Popup core JS file -->
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/magnific-popup/jquery.magnific-popup.js"></script>		
		<!-- Owl Carousel --> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/owl-carousel/owl.carousel.js"></script>
        <!-- FitVids --> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/fitvids/jquery.fitvids.js"></script>
		<!-- ScrollTo --> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/nav/jquery.scrollTo.js"></script> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/nav/jquery.nav.js"></script>
		<!-- Sticky --> 
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/sticky/jquery.sticky.js"></script>		
		<!-- Custom JS -->
		<script src="<?= base_url() ?>assets/frontend/js/custom/custom.js"></script>
	</body>
</html>