<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>LaFORZA</title>
		<!-- All Stylesheets -->
		<link href="<?= base_url() ?>assets/frontend/css/all-stylesheets.css" rel="stylesheet">
		<!-- ALL COLORED STYLESHEETS -->
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/blue.css" title="default">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/green.css" title="green">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/red.css" title="red">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/orange.css" title="orange">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/pink.css" title="pink">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
				<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<![endif]-->
	</head>
	<body>