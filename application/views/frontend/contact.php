			========================================================================= -->
		<!-- CONTACTS CONTENTS STARTS
			========================================================================= -->
		<div class="contacts">
			<div class="container">
				<div class="row">
					<!-- Left Column Starts -->
					<div class="col-lg-7 col-md-7 form">
						<h1>We will be happy to answer</h1>
						<div class="description">Rhythm Yoga has brought a name to the middle-east as the First Yoga centre with an accreditation from TUV Rheinland as an ISO 9001;2008 Certified centre for wellness ,through the science of ancient scientific knowledge applicable to modern living with a Universal application to help every living human being in the whole world. 
						</div>
						<!-- Form Starts -->
						<form action='Staticpages/submitContact' method='post' name='ContactForm' id='ContactForm' >
							<div class="row">
								<div class="col-lg-6 col-md-6">
									<div class="form-group">    
										<input type="text" class="form-control" name="name" placeholder="Full Name *" required="required">
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group">    
										<input type="email" class="form-control" name="email" placeholder="Email *" required="required">
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group">    
										<input type="text" class="form-control" name="phone" placeholder="Phone">
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group">    
										<input type="text" class="form-control" name="subject" placeholder="Subject *" required="required">
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<textarea rows="5" class="form-control" name="content" placeholder="Your Message *" style="height:175px;" required="required"></textarea>
									</div>
								</div>
								<div class="col-lg-12">
									<div id='message_post'></div>
									<input class="btn btn-default" type='submit' value='SUBMIT' name='submitf' id="submitf">
								</div>
							</div>
						</form>
						<!-- Form Ends -->					
					</div>
					<!-- Left Column Ends -->
					<!-- Rights Column Starts -->
					<div class="col-lg-5 col-md-5">
						<div class="postal-address">
							<h1>Head Office</h1>
							<div class="description">Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero.</div>
							<ul>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-map-marker"></i></div>
									<div class="text"><strong><?=$settings[0]->company_address?></div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="text"><strong><?=$settings[0]->contact_phone?></strong></div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<div class="text"><strong><a href=""><?=$settings[0]->contact_email?></a></strong></div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-clock-o"></i></div>
									<div class="text"><strong><?=$settings[0]->working_hours?></strong></div>
								</li>
							</ul>
						</div>
						<hr>
<!-- 						<div class="postal-address">
							<h1>Branch Office</h1>
							<ul>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-map-marker"></i></div>
									<div class="text"><strong>LaForza training center</strong><br>USA California 900 Samoset Drive. DE1-023-45-67</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="text"><strong>+ 180 09  345 9876</strong></div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<div class="text"><strong><a href="">info@domain.com</a></strong></div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-clock-o"></i></div>
									<div class="text"><strong>Mon - Sat  8.00 - 19.00</strong></div>
								</li>
							</ul>
						</div> -->
					</div>
					<!-- Rights Column Ends -->
				</div>
			</div>
		</div>
		<!-- /. CONTACTS CONTENTS ENDS
			========================================================================= -->
		<!-- GOOGLE MAP STARTS
			========================================================================= -->     
		<div class="container-fluid">
			<div class="row no-gutter-12">
				<div class="col-lg-12 map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d25215.625657884106!2d144.956637!3d-37.81456500000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4e793770d3%3A0x9e44d6ad0d76ba7c!2s121+King+St%2C+Melbourne+VIC+3000%2C+Australia!5e0!3m2!1sen!2sus!4v1435061406583" height="400" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<!-- /. GOOGLE MAP ENDS
			========================================================================= -->
