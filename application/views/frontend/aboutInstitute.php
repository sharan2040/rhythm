		<!-- STORIES CONTENTS STARTS
		========================================================================= -->
		<div class="stories" style="padding-top:90px;">

			<!-- INTRO STARTS
			========================================================================= -->
			<div class="container intro">
				<div class="row">

					<div class="col-lg-12 col-md-12 description"> <strong>RHYTHM YOGA</strong> , is a registered institution established from 2003 under DED, DUBAI, UAE. This Institution of wellness is for every individual/families /societies and specially for children/teenagers and corporates who cares for their wellbeing and for other's peace and happiness in life. 
						Ably holding the reins of this noble institution is Shri. Sanjeev Krishnan, lovingly called Guruji <em>(teacher with respect)</em> by his students across the globe who has benefitted from his various classes and programs for the last more than a decade in the middle east.
						KNOWLEDGE is the foundation for growth in all fields specially onto wellness for individual/societies and the world and  is the key for  physical and mental health. A strong confident mind, courage, drive and enthusiasm to look forward for a dynamic and vibrant life with the foundation and the power of deep inner silence well maintained in the most tiring and challenging situations of modern living.<br><br>

						<strong>This unique  knowledge</strong> 
						is the transcendence of the keen interest he has had from his childhood and from 14 years of age ,under the tutelage of great masters and life experiences, has master minded these effective programs to suit the lives of anyone who is in  thirst for the knowledge on health and wellbeing. 									
						The various programs designed by this Institution has helped people from all walks of life who has come into contact. Be it for any type of psychosomatic health issues ,like diabetes ,asthma ,breathing and lung disorders, obesity ,backaches,spinal issues ,depressions ,stress related issues ,losing confidence,cholesterol ,colitis and very many psychosomatic ailments.
						Relationship and bondings are made healthy be it in  family ,businesses ,job environment ,children ,couples etc, is a unique solution envisaged by this effective sessions .<br><br>

						<font color="#7000A0"><strong><em>Rhythm Yoga has brought a name to the middle-east as the First Yoga centre with an accreditation from TUV Rheinland as an ISO 9001;2008 Certified centre for wellness,through the science of  ancient scientific knowledge applicable to modern living with a universal application to help every living human being in the whole world.</em></strong></font>

					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h3 style="color:#7000A0;"><i class="fa fa-certificate" aria-hidden="true"></i> Certifications</h3>
					</div>
					<div class="col-md-4">
						ISO Certification
					</div>
					<div class="col-md-4">
						New acquired
					</div>
					<div class="col-md-4">
						Any Other
					</div>
				</div>

				<br><br><br><br>
				<div class="row">
					<div class="col-md-12">
						<h3 style="color:#7000A0;"><i class="fa fa-users" aria-hidden="true"></i> Trainers</h3>
					</div>
					<div class="col-md-12">
						<h4>Please forward the profiles of Trainers</h4>
					</div>
				</div>



				<div class="row">
					<div class="col-md-4"><img src="" /></div>
				</div>
			</div>
			<!-- /. INTRO ENDS
			========================================================================= -->

			<!-- BLOCKS STARTS
			========================================================================= -->
		<div class="container index-blocks">
			<!-- Fitness Starts -->
			<div class="row no-gutter-4">
				<div class="rowheight row-height">
					<div class="col-lg-4 col-md-4 clearfix column col-height col-top index-news light-grey-bg">
						<div class="news-carousel clearfix">
						<?php foreach($allNews as $news):?>
							<!-- News Starts -->
							<div class="block clearfix"><br>
								<h4><?= substr($news->title, 0, 50);?>..</h4>
								<div class="picture"><img src="images/news/carousel/1.jpg" class="img-responsive" alt="" ></div>
								<div class="detail clearfix">
									<div class="description"><?= substr(strip_tags($news->contents), 0, 145);?> ...</div>
									<div class="meta"><span class="user"><i class="fa fa-user"></i> <a href="">admin</a></span><span class="date"><i class="fa fa-calendar"></i> <a href="">12 Sep 2015</a></span></div>
								</div>
							</div>
						<?php endforeach;?>	
							<!-- News Ends -->
							
						</div>
					</div>
					
					<div class="col-lg-8 col-md-8 column col-height col-top index-partners light-grey-bg">
						<div class="block">
							<h1>Corporate Clientele</h1>
							<div class="row">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/1.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/2.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/3.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/4.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/5.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/6.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/7.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/8.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/9.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/10a.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/11.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/12.png" class="img-responsive center-block" alt="" width="80px" ></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- /. BLOCKS ENDS
			========================================================================= -->

		


						<!-- OUR PHOTOS STARTS
						========================================================================= -->
						<div class="our-photos">

							<div class="heading-strip" style="background-color:#7000A0;">
								<div class="container">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<h1 align="right"><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp; Memorable Moments</h1>
										</div>

									</div>
								</div>
							</div>

							<div class="container-fluid">
								<div class="row no-gutter-3">
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company1.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company2.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company3.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company4.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company4.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company5.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company5.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company6.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company6.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company7.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company7.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company8.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company8.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company9.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company9.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company10.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company10.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company11.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company11.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company12.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company12.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company13.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company13.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company14.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company14.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company15.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company15.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->
									<!-- Picture Starts -->
									<div class="col-lg-3 col-md-4 col-sm-6 block">
										<div class="picture">
											<img src="<?= base_url()?>assets/frontend/images/institute/company16.jpg" class="img-responsive" width="100%">
											<!-- Picture Overlay Starts -->
											<div class="portfolio-overlay">
												<div class="icons">
													<div><span class="icon"><a class="image-popup-vertical-fit" href="<?= base_url()?>assets/frontend/images/institute/company16.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
												</div>
											</div>
											<!-- Picture Overlay Ends -->
										</div>
									</div>
									<!-- Picture Ends -->


								</div>
							</div>
						</div>
			<!-- /. OUR PHOTOS ENDS
			========================================================================= -->   



			
		</div>
		<!-- /. STORIES CONTENTS ENDS
		========================================================================= -->


