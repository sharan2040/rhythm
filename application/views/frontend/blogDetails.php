
		<!-- BLOG CONTENTS STARTS
			========================================================================= -->
		<div class="blog">
			<div class="container">
				<div class="row">
    				<?php if(!empty($blogDetails)): ?>
					<!-- LEFT COLUMN STARTS
						========================================================================= -->
					<div class="col-lg-8 col-md-8">
						<!-- Post Details Starts -->
						<div class="post-detail">
							<div class="picture">
								<img src="<?=base_url()?>assets/uploads/blogs/<?=$blogDetails[0]->cover_image ?>" class="img-responsive" alt="">
								<div class="date left">
									<div class="day"><?php echo date('d', strtotime($blogDetails[0]->created_date))?></div>
									<div class="month"><?php echo date('M', strtotime($blogDetails[0]->created_date))?></div>
								</div>
							</div>
							<div class="details">
								<h1><?=$blogDetails[0]->title ?></h1>
								<div class="meta"><span class="user"><strong>Author :</strong> Sanjeev Krishnan</span><span class="comment"><a href=""><i class="fa fa-comments"></i>0 Comments</a> | <i class="fa fa-eye"></i> <?=$blogDetails[0]->impressions ?> Views</a></span></div>
								<div class="description">
									<?=$blogDetails[0]->contents ?>
								</div>
							</div>
							<!-- About Author Starts -->
							<!-- About Author Ends -->
							<div class="about-autor">
								<div class="picture"><img src="<?=base_url()?>assets/frontend/images/author.jpg" class="img-responsive" alt="" ></div>
								<div class="information">
									<div class="name">Sanjeev Krishnan</div>
									<div class="dept">Director | Rhythm Yoga</div>
									<div class="description">He is the founder of Rhythm Yoga. Talks and Seminars in various public platforms and Educational Institutions. Also, loves to write blogs &amp; articles during leisure time.
									</div>
								</div>
							</div>
							<!-- Comments Starts -->
							
							<!-- Comments Ends -->
						</div>
						<!-- Post Details Ends -->
					</div>
     				<?php else :?>
						<div class="col-lg-8 col-md-8">
	     					<h3 align="center" style="padding-top:60px;"><i class="fa fa-frown-o" aria-hidden="true"></i> No such Blog</h3>
	     					<h4 align="center">Maybe you followed a wrong/broken link</h4>
	     				</div>
     				<?php endif;?>
					<!-- /. LEFT COLUMN ENDS
						========================================================================= -->
					<!-- RIGHT COLUMN STARTS
						========================================================================= -->
					<div class="col-lg-4 col-md-4">
						<div class="sidebar">
							<!-- Search Starts -->
							<div class="search">
								<div class="searchform3">
									<form id="search3">
										<input type="text" class="s" id="s3" placeholder="Search a blog">
										<button type="submit" class="sbtn3"><i class="fa fa-search"></i></button>
									</form>
								</div>
							</div>
							<!-- Search Ends -->
							
							<!-- Latest News Starts -->
							<div class="latest-news">
								<h1>Recent Blogs</h1>
								<ul>
									<?php foreach($allBlogs as $blog):?>
										<li class="clearfix">
											<div style="background-image: url('<?=base_url()?>assets/uploads/blogs/<?= $blog->cover_image; ?>')!important; height:120px; background-size:cover;"></div>
											<div style="margin-top:7px;">
												<div class="caption"><h4 style="display:inline;"><?= $blog->title; ?> </h4>| <a href="<?=base_url()?>blog/<?= $blog->slug?>">Read More <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
												<div class="comment"><i class="fa fa-comments"></i> 28 Comments | <i class="fa fa-eye"></i> <?= $blog->impressions; ?> Views</div>
											</div>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
							<!-- Latest News Ends -->

							<!-- Videos Starts -->
							<!-- <div class="latest-news">
								<h1>Recent Videos</h1>
								<div class="video-carousel">
									<div class="fitvid">
										<iframe src="http://player.vimeo.com/video/80629469" width="480" height="270" allowfullscreen></iframe>
									</div>
									<div class="fitvid">
										<iframe src="http://player.vimeo.com/video/80629469" width="480" height="270" allowfullscreen></iframe>
									</div>
									<div class="fitvid">
										<iframe src="http://player.vimeo.com/video/80629469" width="480" height="270" allowfullscreen></iframe>
									</div>
								</div>
							</div> -->
							<!-- Videos Ends -->
							<!-- Text Widget Starts -->
							<!-- <div class="text-widget">
								<h1>Text Widget</h1>
								<div class="contents">
									Esllentesque lacus.Vivamus lorem arcu semperd
									duiac. Cras ornare arcu avamus nda leo. Etiam ind
									arcu. Morbi justo mauris tempus pharetrad interd
									um at congue semper purus. Lorem ipsum dolor
									sit amet sed consectetura.
								</div>
							</div> -->
							<!-- Text Widget Ends --> 
						</div>
					</div>
					<!-- /. RIGHT COLUMN ENDS
						========================================================================= -->
				</div>
			</div>
		</div>
		<!-- /. BLOG CONTENTS ENDS
			========================================================================= -->
