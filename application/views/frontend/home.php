
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Rhythm Yoga | Dubai</title>
	<!-- All Stylesheets -->
	<link href="<?= base_url() ?>assets/frontend/css/all-stylesheets.css" rel="stylesheet">
	<!-- ALL COLORED STYLESHEETS -->
	<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/blue.css" title="default">
	<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/green.css" title="green">
	<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/red.css" title="red">
	<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/orange.css" title="orange">
	<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/pink.css" title="pink">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- SLIDER STARTS
		========================================================================= -->		
		<div class="content">
			<?php $this->load->view('themes/main-slider');?>
		</div>
		<!-- SLIDER ENDS
		========================================================================= -->

		<!-- NAVIGATION STARTS
		========================================================================= -->
		<?php $this->load->view('themes/navbar');?>
		<!-- /. NAVIGATION ENDS
		========================================================================= -->

		<!-- ENGAGE STARTS
		========================================================================= -->
		<div class="container engage">
			<div class="row no-gutter-8 no-gutter-4">
				<div class="col-lg-8 col-md-8">
					<h1>we help people  find themselves</h1>
					<div class="description">Many every day looking in the mirror and promise to begin to engage in their  health<br>tomorrow. We will give you the incentive to start</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="blue-bg">
						<h2><strong>START</strong> to engage in sports right now</h2>
					</div>
				</div>
			</div>
		</div>
		<!-- /. ENGAGE ENDS
		========================================================================= -->
		<!-- BLOCKS STARTS
		========================================================================= -->    
		<div class="container index-blocks">
			<!-- Fitness Starts -->
			<div class="row no-gutter-4">
				<div class="rowheight row-height">
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top box light-grey-bg">
						<!-- <h1><i class="flaticon-man276"></i> Flexibility</h1> -->
						<h1><i class="flaticon-heart288"></i> Daily Thoughts</h1>
						<div class="description">Shri. Sanjeev Krishnan affectionately addressed by all as “Guruji” has been from his young age passionate about life and its significance. This led him to search and seek spiritual knowledge.</div>
						<div class="button"><a href="<?= base_url()?>guruji-sanjeev">About Guruji ></a></div>
					</div>
					<!-- Column Ends -->
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top"><img src="<?= base_url() ?>assets/frontend/images/fitness/guruji.jpg" class="img-responsive" alt="" ></div>
					<!-- Column Ends -->
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top box blue-bg course">
						
						<div class="description"> ".          As you seek.   "
Knowledge cannot hold back
From a true seeker of truth,
Who craves from the heart,
For  to His innocent Child.." to His innocent Child.." knowledge is love ,
Of God to His innocent Child.."
to His innocent Child.."
As a transcendence of divine nectar 
To quench the thirst and relieves
to His innocent Child.."
HIS child from all sufferings. <br>SANYOG Darshan</div>
						<div class="button"><a style="background-color:#FFF" href="<?= base_url()?>guruji-sanjeev">View More ></a></div>
					</div>
					<!-- Column Ends -->
				</div>
			</div>
			<!-- Fitness Starts -->
			<!-- Stretch Yoga Starts -->
			<div class="row no-gutter-4 no-gutter-8">
				<div class="rowheight row-height">
					<!-- Column Starts -->
					<div class="col-lg-8 col-md-8 column col-height col-top"><img src="<?= base_url()?>assets/uploads/programs/<?= $randomProgram->cover_image?>" class="img-responsive" alt="" ></div>
					<!-- Column Ends -->
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top box light-grey-bg">
						<h1><i class="flaticon-buddhism2"></i> <?= $randomProgram->title?></h1>
						<div class="description"><?= substr(strip_tags($randomProgram->description),0 , 180)?>....</div>
						<div class="button"><a href="<?= base_url().'programmes/'.$randomProgram->slug?>"><i class="fa fa-book" aria-hidden="true"></i> Know More</a></div>
					</div>
					<!-- Column Ends -->
				</div>
			</div>
			<!-- Stretch Yoga Starts -->
			
			<!-- CrossFit Starts -->
			<div class="row no-gutter-4">
				<div class="rowheight row-height">
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top box blue-bg course">
						<div class="description">Kalari is a traditional psycho-physiological discipline emanating from Kerala’s unique mytho-historical heritage and considered to be the oldest form of physical training in human history. The earliest mention of Kalari can be traced back to the Dhanur Vedic texts dating back to 3000 BC and Vishnu Puruna, which describes Kalari as one of eighteen branches of knowledge. </div>
						
					</div>
					<!-- Column Ends -->
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top box light-grey-bg">
						<h1><i class="flaticon-martial2"></i> Kalaripayattu</h1>
						<div class="description">A holistic form of physical training, which combines the dynamic skills of attack and defense with the secret knowledge of the Marma points. </div>
						<div class="button"><a href=""><i class="fa fa-fire" aria-hidden="true"></i> Enroll Today</a></div>
					</div>
					<!-- Column Ends -->
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top"><img src="http://localhost/rhythm/assets/frontend/images/fitness/kalari.jpg" class="img-responsive" alt="" ></div>
					<!-- Column Ends -->
				</div>
			</div>
			<!-- CrossFit Starts -->
			<!-- Fitbox Starts -->
			<div class="row no-gutter-4 no-gutter-8">
				<div class="rowheight row-height">
					<!-- Column Starts -->
					<div class="col-lg-8 col-md-8 column col-height col-top"><img src="<?= base_url()?>assets/uploads/blogs/<?= $blog->cover_image?>" class="img-responsive" alt="" ></div>
					<!-- Column Ends -->
					<!-- Column Starts -->
					<div class="col-lg-4 col-md-4 column col-height col-top box light-grey-bg">
						<h1><i class="flaticon-clenched"></i> Blogs &amp; Articles</h1>
						<div class="description"><?= substr(strip_tags($blog->contents),0 , 225)?>.... </div>
						<div class="button"><a style="background-color:#7000A0; color:#fff" href="<?= base_url().'blog/'.$blog->slug?>">Read More <i class="fa fa-caret-square-o-right" aria-hidden="true"></i></a> <a href="<?= base_url().'blogs'?>"><i class="fa fa-list" aria-hidden="true"></i> View all Blogs</a></div>
					</div>
					<!-- Column Ends -->
				</div>
			</div>
			<!-- Fitbox Starts -->
			
			
		</div>
		<!-- /. BLOCKS ENDS
		========================================================================= -->
		<!-- BLOCKS STARTS
		========================================================================= -->
		<div class="container index-blocks">
			<!-- Fitness Starts -->
			<div class="row no-gutter-4">
				<div class="rowheight row-height">
					<div class="col-lg-4 col-md-4 clearfix column col-height col-top index-news light-grey-bg">
						<div class="news-carousel clearfix">
							<!-- News Starts -->
							<div class="block clearfix">
								<h1>Notifications</h1>
								<!-- <div class="picture"><img src="images/news/carousel/1.jpg" class="img-responsive" alt="" ></div> -->
								<div class="detail clearfix">
									<div class="description">Workouts include elements of high intensity interval training, weightlifting, athletics, plyometrics, power lifting, gymnastics, weight-lifting.</div>
									<div class="meta"><span class="user"><i class="fa fa-user"></i> <a href="">admin</a></span><span class="date"><i class="fa fa-calendar"></i> <a href="">12 Sep 2015</a></span></div>
								</div>
							</div>
							<!-- News Ends -->
							<!-- News Starts -->
							<div class="block clearfix">
								<h1>News</h1>
								<div class="picture"><img src="images/news/carousel/1.jpg" class="img-responsive" alt="" ></div>
								<div class="detail clearfix">
									<div class="description">Workouts include elements of high intensity interval training, weightlifting, athletics, plyometrics, power lifting, gymnastics, weight-lifting.</div>
									<div class="meta"><span class="user"><i class="fa fa-user"></i> <a href="">admin</a></span><span class="date"><i class="fa fa-calendar"></i> <a href="">12 Sep 2015</a></span></div>
								</div>
							</div>
							<!-- News Ends -->
						</div>
					</div>
					
					<div class="col-lg-8 col-md-8 column col-height col-top index-partners light-grey-bg">
						<div class="block">
							<h1>Corporate Clientele</h1>
							<div class="row">
								<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/1.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/2.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/3.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/4.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/5.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/6.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/7.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/8.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/9.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/10a.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/11.png" class="img-responsive center-block" alt="" width="80px" ></div>
								<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"><img src="<?= base_url() ?>assets/frontend/images/corporate-logos/12.png" class="img-responsive center-block" alt="" width="80px" ></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- /. BLOCKS ENDS
		========================================================================= -->
		<!-- STRIP STARTS
		========================================================================= -->
		<div class="totop-strip blue-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 footer-line">First ISO Certified Yoga Centre in the Middle East | Working Hours : <?= $settings[0]->working_hours; ?></div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
						<div class="scrollup"><a href="#"></a></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /. STRIP ENDS
		========================================================================= -->
		<!-- FOOTER STARTS
		========================================================================= -->
		<footer class="dark-grey-bg">
			<div class="container">
				<div class="row">
					<!-- Stories Starts -->
					<div class="col-lg-4 clearfix f-stories">
						<div class="stories-carousel">
							<!-- Story Starts -->
							<div class="block">
								<h1>Testimonials</h1>
								<div class="picture">
									<img src="<?= base_url() ?>assets/frontend/images/news/carousel/1.jpg" class="img-responsive" alt="" >
									<div class="name">Eric<br>Marcovich</div>
								</div>
								<div class="description">Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. </div>
							</div>
							<!-- Story Ends -->
							<!-- Story Starts -->
							<div class="block">
								<h1>Testimonials</h1>
								<div class="picture">
									<img src="<?= base_url() ?>assets/frontend/images/news/carousel/1.jpg" class="img-responsive" alt="" >
									<div class="name">Eric<br>Marcovich</div>
								</div>
								<div class="description">Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. </div>
							</div>
							<!-- Story Ends -->
							<!-- Story Starts -->
							<div class="block">
								<h1>Testimonials</h1>
								<div class="picture">
									<img src="<?= base_url() ?>assets/frontend/images/news/carousel/1.jpg" class="img-responsive" alt="" >
									<div class="name">Eric<br>Marcovich</div>
								</div>
								<div class="description">Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. </div>
							</div>
							<!-- Story Ends -->
						</div>
					</div>
					<!-- Stories Ends -->
					<!-- Links Starts -->
					<div class="col-lg-5 links">
						<div class="block">
							<h1>Pages</h1>
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
									<ul class="links">
										<li><a href="">Home</a></li>
										<li><a href="">Events</a></li>
										<li><a href="">Stories</a></li>
										<li><a href="">Articles</a></li>
									</ul>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
									<ul class="links">
										<li><a href="">Intro</a></li>
										<li><a href="">Featured</a></li>
										<li><a href="">Plan</a></li>
										<li><a href="trainer.html" class="external">Trainer</a></li>
										<li><a href="stories.html" class="external">Stories</a></li>
									</ul>
								</div>
								<div class="col-lg-5 instagram">
									<!-- SnapWidget -->
									<script src="http://snapwidget.com/js/snapwidget.js"></script>
									<iframe src="http://snapwidget.com/in/?h=ZW52YXRvc3R1ZGlvfGlufDQwfDJ8Mnx8bm98OHxmYWRlT3V0fG9uU3RhcnR8eWVzfHllcw==&amp;ve=121115" title="Instagram Widget" class="snapwidget-widget" style="border:none; overflow:hidden; width:100%;"></iframe>
									Instagram Photos
								</div>
							</div>
						</div>
					</div>
					<!-- Links Ends -->
					<!-- Contact Starts -->
					<div class="col-lg-3 contact">
						<div class="block">
							<h1>Contact us</h1>
							<ul>
								<li>
									<div class="icon"><i class="fa fa-phone"></i></div>
									<div class="text">Telephone:  <?= $settings[0]->contact_phone; ?></div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-fax"></i></div>
									<div class="text">Fax/phone:  <?= $settings[0]->contact_fax; ?></div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<div class="text">E-mail:  <a href=""><?= $settings[0]->contact_email; ?></a></div>
								</li>
								<li>
									<div class="icon"><i class="fa fa-map-marker"></i></div>
									<div class="text">Address: <?= $settings[0]->company_address; ?></div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Contact Ends -->
				</div>
			</div>
		</footer>
		<!-- /. FOOTER ENDS
		========================================================================= -->
		<!-- COPYRIGHT STARTS
		========================================================================= -->
		<div class="copyright medium-grey-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">Copyright © <?= date('Y')?> Rhythm Yoga | All Rights Reserved | <a href="">Terms &amp; Conditions</a> | Powered by <a href="http://www.codecub.in/" target="_blank"><img src="<?=base_url()?>assets/codecub.png" style="display: inline; height: 15px;"> CodeCub Softlabs</div>
					<div class="col-lg-4">
						<ul class="social-icons">
							<li>Social Links:</li>
							<li><a href=""><i class="fa fa-facebook"></i></a></li>
							<li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href=""><i class="fa fa-instagram"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /. COPYRIGHT ENDS
		========================================================================= -->
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?= base_url() ?>assets/frontend/js/bootstrap.min.js"></script>
		<!-- REVOLUTION JS FILES -->
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/jquery.themepunch.revolution.min.js"></script>
		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
			(Load Extensions only on Local File Systems !  
			The following part can be removed on Server for On Demand Loading) -->	
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.actions.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.migration.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/revolution/js/extensions/revolution.extension.video.min.js"></script>
			<script type="text/javascript">
				var tpj=jQuery;			
				var revapi4;
				tpj(document).ready(function() {
					if(tpj("#rev_slider_46_1").revolution == undefined){
						revslider_showDoubleJqueryError("#rev_slider_46_1");
					}else{
						revapi4 = tpj("#rev_slider_46_1").show().revolution({
							sliderType:"standard",
							jsFileLocation:"revolution/js/",
							sliderLayout:"fullscreen",
							dottedOverlay:"none",
							delay:9000,
							navigation: {
								keyboardNavigation:"off",
								keyboard_direction: "horizontal",
								mouseScrollNavigation:"off",
								onHoverStop:"off",
								touch:{
									touchenabled:"on",
									swipe_threshold: 75,
									swipe_min_touches: 1,
									swipe_direction: "horizontal",
									drag_block_vertical: false
								}
								,
								arrows: {
									style:"zeus",
									enable:true,
									hide_onmobile:true,
									hide_under:600,
									hide_onleave:true,
									hide_delay:200,
									hide_delay_mobile:1200,
									tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
									left: {
										h_align:"left",
										v_align:"center",
										h_offset:30,
										v_offset:0
									},
									right: {
										h_align:"right",
										v_align:"center",
										h_offset:30,
										v_offset:0
									}
								}
								,
								bullets: {
									enable:true,
									hide_onmobile:true,
									hide_under:600,
									style:"metis",
									hide_onleave:true,
									hide_delay:200,
									hide_delay_mobile:1200,
									direction:"horizontal",
									h_align:"center",
									v_align:"bottom",
									h_offset:0,
									v_offset:30,
									space:5,
									tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
								}
							},
							viewPort: {
								enable:true,
								outof:"pause",
								visible_area:"80%"
							},
							responsiveLevels:[1240,1024,778,480],
							gridwidth:[1240,1024,778,480],
							gridheight:[600,600,500,400],
							lazyType:"none",
							parallax: {
								type:"mouse",
								origo:"slidercenter",
								speed:2000,
								levels:[2,3,4,5,6,7,12,16,10,50],
							},
							shadow:0,
							spinner:"off",
							stopLoop:"off",
							stopAfterLoops:-1,
							stopAtSlide:-1,

							fullScreenAlignForce:"off",
							fullScreenOffsetContainer: "",
							fullScreenOffset: "0px",
							disableProgressBar:"on",
							hideThumbsOnMobile:"off",

							shuffle:"off",
							autoHeight:"off",
							hideThumbsOnMobile:"off",
							hideSliderAtLimit:0,
							hideCaptionAtLimit:0,
							hideAllCaptionAtLilmit:0,
							debugMode:false,
							fallbacks: {
								simplifyAll:"off",
								nextSlideOnWindowFocus:"off",
								disableFocusListener:false,
							}
						});
					}
				});	/*ready*/
			</script>

			<!-- Isotope Gallery --> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/isotope/jquery.isotope.min.js"></script> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/isotope/custom-isotope-mansory.js"></script>
			<!-- Magnific Popup core JS file -->
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/magnific-popup/jquery.magnific-popup.js"></script>		
			<!-- Owl Carousel --> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/owl-carousel/owl.carousel.js"></script>
			<!-- FitVids --> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/fitvids/jquery.fitvids.js"></script>
			<!-- ScrollTo --> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/nav/jquery.scrollTo.js"></script> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/nav/jquery.nav.js"></script>
			<!-- Sticky --> 
			<script type="text/javascript" src="<?= base_url() ?>assets/frontend/js/sticky/jquery.sticky.js"></script>		
			<!-- Custom JS -->
			<script src="<?= base_url() ?>assets/frontend/js/custom/custom.js"></script>
		</body>
		</html>