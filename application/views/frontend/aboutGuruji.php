		<!-- STORIES CONTENTS STARTS
		========================================================================= -->
		<div class="stories" style="padding-top:110px;">

			<!-- INTRO STARTS
			========================================================================= -->
			<div class="container intro">

				
				
				<div class="row">
					<div class="col-md-12">
						<img src="<?= base_url()?>assets/frontend/images/guruji-wide.jpg" width="100%"/>
					</div>
				</div>

				

				<div class="row">

					<div class="col-lg-12 col-md-12 description"> 

						<p><strong>Shri.  Sanjeev Krishnan</strong> affectionately addressed by all as <strong>“Guruji”</strong> has been from his young age passionate about life and its significance. This led him to search and  seek spiritual knowledge.</p>
						<p>At the young age of 12, when normal children were interested only in their routine daily activities he was in search of a Master for Guidance.  His quest for spiritual knowledge for the betterment of mankind  drew  the  attention of one of the greatt Master to him “”Shri. Thankappan””, a direct disciple of Swami SivanandaParamhansa, a Siddha yogi [enlightenend teacher] of the highest of  spiritual order from the South of India .Under his tutelageand from various other lineage of great Masters  under whom he took up teachers training in various dimensions of this invaluable knowledge of the self and its applications in modern life.</p>
						<p>Today with his vast knowledge on the subject and the training imparted for the last four decades of his life has again made   him realize that knowledge is like a vast ocean. “The more you learn the less you know is what””Guruji“”feels. He is now  trying  to  introduce the  oldest  from  of  defence in Indian culture (kalari)  as  a  way  to  instill  values  and  discipline  into  human life  and  to  harness  the  excess energy in  children for  improving their  overall  mental and  physical development.  Kalari inculcates  focus, determination and  aim in  the  minds  of  people  practicing  it.</p>
						<p>Graduated in English language and literature, and higher education in professional line in the respective fields from time to time has been exposed to various fields  , to  cite  few; Medical, Transport, Finance /Banking. HR Director and to top it  all as   Regional Head in Financial Institutions . He has undergone and experienced life in various corporate exposure in the most strenuous of situations.</p>
						<p>With a backing of nearly four decades of knowledge in the field of Yogic science, as a student, trainer and as a practioner in this unique knowledge. There came a  time when “ Guruji” finally decided to pursue his passion which had been a true companion in all facets of his life .With the grace of all Great  Masters and well-wishersfinally pursued his passion by setting up the Rhythm Yoga in Dubai by the grace of providence is what “Guruji” feels  in 2003.</p>
						<p>Now in Dubai, since more than a  decade  in the name of “”Rhythm Yoga“” he has conducted classes for Individuals, Corporate Bodies, Educational Institutions and Universities with frequent  talks, seminars and classes.
						His clientele includes  common  man  and  many Corporate bodies like ENOC, KHDA, Maersk Sealand, Master foods [MARS GCC], Orient Exchange, Touchwood Décor and Furniture, Landmark Group, RHS, Gems Group of Educational Institutions, etc.
						Has formulated a popular  program called the RHYTHM Of Life which is a life changing and self management program for rectifying all the physical and mental imbalances, and it has cured and treated people with very many disorders in various psychosomatic health issues just by simple practices on breathing, food and yogic practices.<br>
						More than 6000 members have undergone this program across the globe and now the regular chapters are going on in the centre in Dubai.</p>

						<p><font color="#7000A0"><strong><em>Sanjeev is also a faculty in American University in Dubai for teaching yoga to the staff and the students of the college.</em></strong></font>

						<ul>
							<li>Received participation award for participating in the ‘Contribution to the world health’ program conducted by the Department of Health and medical services [DOHMS], DUBAI, UAE during DSF 2004.</li>
							<li>Conducts classes for various corporate companies on health and personal management.</li>
							<li>Talks and Seminars in various public platforms and Educational Institutions ,etc.</li>
							<li>Participated in the ‘Keep Fit’ program conducted for the staff of Emirates airlines.</li>
							<li>Thousands of students from across the globe has undergone and benefitted from his programs</li>
						</ul>
						</p>						


					</div>
				</div>



			</div>
			<!-- /. INTRO ENDS
			========================================================================= -->







			
		</div>
		<!-- /. STORIES CONTENTS ENDS
		========================================================================= -->


