		<!-- CLASSES CONTENTS STARTS
		========================================================================= -->
		<div class="classes-details">
			<div class="container">
				<!-- <div class="row intro">
					<div class="col-lg-12">
						<h1 align="center"><?= $programmes[0]->main_caption?></h1>
						<h2 align="center"><?= $programmes[0]->sub_caption?></h2>
					</div>
				</div> -->
				<!-- Pictures Carousel Starts -->
				<div class="row" style="padding-top:10px;padding-bottom:15px;">
					<div class="col-lg-12">
						<img src="<?= base_url()?>assets/uploads/programs/<?= $programmes[0]->cover_image?>" class="img-responsive" width="100%" alt="">
					</div>
				</div>
				<!-- Pictures Carousel Ends -->

				<!-- Description Starts -->
				<div class="row">
					<div class="col-lg-12 col-md-12 description"><?= $programmes[0]->description?></div>
				</div>
				<!-- Description Ends -->

				<div class="row">
					<div class="col-md-6">
						<h3 style="color:#7000A0"><i class="fa fa-clock-o" aria-hidden="true"></i> Class Timings</h3>
						<p><?= $programmes[0]->class_timings?></p>
					</div>
				
					<div class="col-md-6">
						<h3 style="color:#7000A0"><i class="fa fa-credit-card" aria-hidden="true"></i> Packages &amp; Rates</h3>
						<p><?= $programmes[0]->packages_and_rates?></p>
					</div>
				</div>

				
			</div>
		</div>

			<!-- CLIENT STORIES STARTS
			========================================================================= -->
			<div class="testimonial" style="padding-top:15px; padding-bottom:15px;">
				<div class="blue-bg heading-strip">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-8">
								<h1>Student Testimonials</h1>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
								<div class="button"><a href="" class="white" data-toggle="modal" data-target="#myModal">Already enrolled? Send us your story</a></div>
							</div>
						</div>
					
					</div>
				</div>
				<?php if(!empty($testimonials)):?>
				<div class="dark-grey-bg-2">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 testimonial-carousel">

								<?php foreach($testimonials as $testimonial):?>
									<?php if($testimonial->status == 1): ?>
										<!-- TESTIMONIAL STARTS -->
										<div class="block clearfix">
											<div class="description">
												<?= $testimonial->content;?>
												<div class="quote"></div>
												<div class="down-arrow"></div>
											</div>
											<div class="clearfix"></div>
											<div class="details clearfix">
												<div class="info">
													<div class="name"><?= $testimonial->student_name;?></div>
													<div class="company">Happy Client</div>
												</div>
												<div class="picture"><img src="<?= base_url()?>assets/frontend/images/testimonials/1.jpg" class="img-responsive" alt="" ></div>
											</div>
										</div>
										<!-- TESTIMONIAL ENDS -->
									<?php endif; ?>
								<?php endforeach; ?>

							</div>
						</div>
						
					</div>
				</div>
				<?php endif; ?>

			</div>
			<!-- /. CLIENT STORIES ENDS
			========================================================================= --> 
			<!-- TEAM STARTS
			========================================================================= -->     
			<div class="team" style="padding-bottom:20px;">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-md-8">
							<h1>Choose a trainer for the soul</h1>
							<div class="description">Only we have the Professional instructors all have merit at least master of sports in his sport. It must be remembered that the right coach is half of your future success.</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="blue-bg">
								<h2><strong>sign up</strong> for a trial lesson</h2>
							</div>
						</div>
					</div>


					
				</div>
			</div>
			<!-- /. TEAM ENDS
			========================================================================= -->



			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content modal-frontend">
						<form role="form" action="<?php echo base_url()?>staticpages/submitTestimonial" method="POST">
							<input type="hidden" name="mainProgramId" value="<?= $programmes[0]->main_programme_id ?>">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title" align="center"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Submit your Testimonial</h4>
							</div>
							<div class="modal-body">

								<div class="form-group">
									<label for="email">Full Name</label>
									<input type="text" class="form-control" id="email" name="name">
								</div>

								<div class="form-group">
									<label for="email">Your Email</label>
									<input type="email" class="form-control" id="email" name="email">
								</div>

								<div class="form-group">
									<label for="pwd">Your Story</label>
									<textarea name="story" placeholder="Please try to limit this below 450 characters" class="form-control" rows="6" required="" maxlength="450"></textarea>
								</div>

								<div class="form-group">
									<i class="fa fa-info-circle" aria-hidden="true"></i> Testimonial will be visible only after approval
								</div>

							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-success">Submit</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>

				</div>
			</div>
