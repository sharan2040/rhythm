				========================================================================= -->
			<!-- CLIENT STORIES STARTS
				========================================================================= -->
			<div class="testimonial">
				<div class="blue-bg heading-strip">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-8">
								<h1>Client Stories</h1>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
								<div class="button"><a href="" class="white">Send your Story</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="dark-grey-bg-2 testimonials">
					<div class="container">
						<div class="row">
							<!-- TESTIMONIAL STARTS -->
							<div class="col-lg-6 col-md-6">
								<div class="block clearfix">
									<div class="description">
										Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis vestibulum suscipit.  
										<div class="quote"></div>
										<div class="down-arrow"></div>
									</div>
									<div class="clearfix"></div>
									<div class="details clearfix">
										<div class="info">
											<div class="name">Jack Rassel</div>
											<div class="company">Happy Client</div>
										</div>
										<div class="picture"><img src="<?=base_url()?>assets/frontend/images/testimonials/1.jpg" class="img-responsive" alt="" ></div>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL ENDS -->
							<!-- TESTIMONIAL STARTS -->
							<div class="col-lg-6 col-md-6">
								<div class="block clearfix">
									<div class="description">
										Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis vestibulum suscipit.  
										<div class="quote"></div>
										<div class="down-arrow"></div>
									</div>
									<div class="clearfix"></div>
									<div class="details clearfix">
										<div class="info">
											<div class="name">Jack Rassel</div>
											<div class="company">Happy Client</div>
										</div>
										<div class="picture"><img src="<?=base_url()?>assets/frontend/images/testimonials/2.jpg" class="img-responsive" alt="" ></div>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL ENDS -->
							<!-- TESTIMONIAL STARTS -->
							<div class="col-lg-6 col-md-6">
								<div class="block clearfix">
									<div class="description">
										Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis vestibulum suscipit.  
										<div class="quote"></div>
										<div class="down-arrow"></div>
									</div>
									<div class="clearfix"></div>
									<div class="details clearfix">
										<div class="info">
											<div class="name">Jack Rassel</div>
											<div class="company">Happy Client</div>
										</div>
										<div class="picture"><img src="<?=base_url()?>assets/frontend/images/testimonials/3.jpg" class="img-responsive" alt="" ></div>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL ENDS -->
							<!-- TESTIMONIAL STARTS -->
							<div class="col-lg-6 col-md-6">
								<div class="block clearfix">
									<div class="description">
										Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis vestibulum suscipit.  
										<div class="quote"></div>
										<div class="down-arrow"></div>
									</div>
									<div class="clearfix"></div>
									<div class="details clearfix">
										<div class="info">
											<div class="name">Jack Rassel</div>
											<div class="company">Happy Client</div>
										</div>
										<div class="picture"><img src="<?=base_url()?>assets/frontend/images/testimonials/4.jpg" class="img-responsive" alt="" ></div>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL ENDS -->
							<!-- TESTIMONIAL STARTS -->
							<div class="col-lg-6 col-md-6">
								<div class="block clearfix">
									<div class="description">
										Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis vestibulum suscipit.  
										<div class="quote"></div>
										<div class="down-arrow"></div>
									</div>
									<div class="clearfix"></div>
									<div class="details clearfix">
										<div class="info">
											<div class="name">Jack Rassel</div>
											<div class="company">Happy Client</div>
										</div>
										<div class="picture"><img src="<?=base_url()?>assets/frontend/images/testimonials/5.jpg" class="img-responsive" alt="" ></div>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL ENDS -->
							<!-- TESTIMONIAL STARTS -->
							<div class="col-lg-6 col-md-6">
								<div class="block clearfix">
									<div class="description">
										Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis vestibulum suscipit.  
										<div class="quote"></div>
										<div class="down-arrow"></div>
									</div>
									<div class="clearfix"></div>
									<div class="details clearfix">
										<div class="info">
											<div class="name">Jack Rassel</div>
											<div class="company">Happy Client</div>
										</div>
										<div class="picture"><img src="<?=base_url()?>assets/frontend/images/testimonials/6.jpg" class="img-responsive" alt="" ></div>
									</div>
								</div>
							</div>
							<!-- TESTIMONIAL ENDS -->
						</div>
					</div>
				</div>
			</div>
			<!-- /. CLIENT STORIES ENDS
				========================================================================= -->    