    <!-- TRAINER CONTENTS STARTS
            ========================================================================= -->
        <div class="trainer">


            <div class="container">
                
                <?php  foreach ($array['items'] as $playlist) :?>
<!--                     <?php print_r($playlist);?>
 -->
                    <!-- Trainer Block Starts -->
                    <div class="row block">
                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                            <div class="picture"><img src="<?= $playlist['snippet']['thumbnails']['high']['url']?>" class="img-responsive" alt="" ></div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                            <div class="info">
                                <div class="dept"><?= $playlist['snippet']['title']?></span></div>
                                <h5>Published on : <?= $playlist['snippet']['publishedAt']?></h5>
                                <div class="description"><?= $playlist['snippet']['description']?></div>
                                <form action="<?= base_url().'video-list'?>" method="POST">
                                    <input type = "hidden" value="<?= $playlist['id']?>" name="playlistId" />
                                    <input type = "hidden" value="<?= $playlist['snippet']['title']?>" name="playlistTitle" />

                                    <button type="submit" class="btn btn-success btn-sm" style="margin-top:10px;"><strong>View Vids >></strong></button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                    <!-- Trainer Block Ends -->

                <?php endforeach; ?>
                
                
            </div>
        </div>
        <!-- /. TRAINER CONTENTS ENDS
            ========================================================================= -->


