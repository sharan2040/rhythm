		<!-- TRAINER CONTENTS STARTS
			========================================================================= -->
		<div class="trainer">
			<div class="container">
				<div class="row intro">
					<div class="col-lg-12">
						<h1>Centres</h1>
						<h2>Etiam rhoncus. Maecenas tempus</h2>
					</div>
				</div>
          		<?php if($centres) :?>
					<!-- Trainer Block Starts -->
					<?php foreach($centres as $centre):?>
						<div class="row block">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="picture"><img src="" class="img-responsive" alt="" ></div>
							</div>
							<div class="col-lg-5 col-md-5">
								<div class="info">
									<div class="dept"><?=$centre->centre_name?><span class="name">Rhythm Yoga</span></div>
									<div class="description"><p>Rhythm of Life yoga centre was started in January 2003 at Karama in Dubai, UAE by Shri. Sanjeev Krishnan. The programs are designed to fine tune your physical and mental health and improve your personal and professional relationships with others. Regular classes are conducted for all disciplines of yoga.</p></div>
									<ul class="awards">
										<li><i class="fa fa-phone"></i><strong><?=$centre->contact_number?></strong></li>
										<li><i class="fa fa-map-marker"></i> <strong><?=$centre->centre_address?></strong></li>
									</ul>
									<ul class="social-icons">
										<li><a href=""><i class="fa fa-facebook"></i></a></li>
										<li><a href=""><i class="fa fa-twitter"></i></a></li>
										<li><a href=""><i class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>	
					<?php endforeach;?>
				<?php else :?>
					<br>
					<h3 align="center"><i class="fa fa-frown-o" aria-hidden="true"></i> Oh Snap! No Details of Centres</h3>
					<h4 align="center">We shall update soon</h4>
					<br>
				<?php endif; ?>	

				<div class="row">
					<div class="col-lg-12">
						<div class="big-button"><a href="<?= base_url()?>contact" class="blue-fill"><strong>Contact</strong> Us</a></div>
					</div>
				</div>

			</div>
		</div>
		<!-- /. TRAINER CONTENTS ENDS
			========================================================================= -->
