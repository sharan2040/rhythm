		<!-- STORIES CONTENTS STARTS
			========================================================================= -->
		<div class="stories">
			<!-- INTRO STARTS
				========================================================================= -->

			<!-- Pictures Carousel Starts -->
			<div class="row" style="padding-top:10px;padding-bottom:15px;">
				<div class="col-lg-12">
					<img src="<?= base_url()?>assets/guruji_wide.jpg" class="img-responsive" width="100%" alt="">
				</div>
			</div>
			<!-- Pictures Carousel Ends -->


			<div class="container intro">
				<div class="row">
					<div class="col-lg-12">
						<h1>Guruji Sanjeev Krishnan</h1>
						<h2>DIRECTOR & YOGA INSTRUCTOR</h2>
					</div>
<!-- 					<div class="col-lg-6 col-md-6">
 -->						<div class="col-lg-12 col-md-12 description"><strong>Sanjeev Krishnan</strong> affectionately called <strong><em>Guruji</em></strong> by his students, from a very young age was deeply interested in the field of spiritual knowledge and his thirst for this great knowledge lead him to great masters in the field who guided him in the respective direction .At the age of fourteen he came in touch with a direct disciple of Swami Sivananda,the siddha Yogi of the south of India, <em>Shri Thankappan</em>. From Him started his learning in a more intense manner into the sacred art of self discipline. From then on he met and had the opportunity to be with many great masters in his search and fullfillment to attain this knowledge in depth .&#8221; Knowledge in this field in like an ocean, the more you know the less you have learnt is what I feel&#8221; says Guruji.</p>
<p>This lovely feeling that he attained made him realise that this knowledge will lead the whole world to immense peace ,good health and happiness if it can be well communicated in the most scientific pattern. Hence this urge has instilled in him to ensure that every human being become aware of this wonderful self knowledge with a scientific approach which will lead him to formulate the popular program by name the <strong>RHYTHM OF LIFE</strong>.</p>
<p>Every Human being is the world has the truth within him. Ignorance of that truth leads one to indulge in various unhealthy and self destructive methods for the individual himself and the world around him.</p>
<p>Peace and happiness can never be achieved by war or by instilling fear in the life of people. Every human being is made up of the same componets of flesh and blood and none is superior or inferior in this world . The only difference is ignorance of this bare truth.Love and knowledge alone can bring in peace and wellness to the world.</p>

<h5><em> All individuals are in their own life rhythm, a bit of fine tuning will accelerate and develop every life that comes in touch with this knowledge.</em></h5>

<p>In twelve days a person can experience the true essence of harmony [YOGA ]as a practice, in daily life, and be relieved from various psychosomatic ailments,which being the imbalances alone that destroys life in all respects.. These principles of management in health, personal and social can lead one to greater heights in materialistic and spiritual growth to realize and tap the highest potential dormant in every human life and will help individual to have a balanced outlook.</p>

<h4> Sanjeev is also a faculty in American University in Dubai for teaching yoga to the staff and the students of the college.</h4>

Received participation award for participating in the ‘Contribution to the world health’ program conducted by the Department of Health and medical services [DOHMS], DUBAI, UAE during DSF 2004.<br>
Conducts classes for various corporate companies on health and personal management.<br>
Talks and Seminars in various public platforms and Educational Institutions ,etc.<br>
Participated in the ‘Keep Fit’ program conducted for the staff of Emirates airlines.<br>
Thousands of students from across the globe has undergone and benefitted from his programs<br>
						</div>
<!-- 					</div>
 -->				</div>
			</div>
			<!-- /. INTRO ENDS
				========================================================================= -->
			
		</div>
		<!-- /. STORIES CONTENTS ENDS
			========================================================================= -->
