<div class="trainer" style="background-color: #000; padding-bottom: 0px;">

	<div class="our-photos">
		<div class="container-fluid">
			<div class="row no-gutter-3">


				<?php 
				foreach ($array as $key => $value) {
					if ($key == "items") {
						for ($i = 0; $i < count($array["items"]); $i = $i + 1) {
							$id = $array["items"][$i]['snippet']['resourceId']['videoId'];
							$thumbnail = $array["items"][$i]['snippet']['thumbnails']['high']['url'];
							$title = $array["items"][$i]['snippet']['title'];
							$description = $array["items"][$i]['snippet']['description'];
							echo '<div class="col-md-3">';
							echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
							echo '</div>';
						}
					}
				}
				?>


			</div>
		</div>
	</div>
</div>
