			========================================================================= -->
		<!-- BLOG CONTENTS STARTS
			========================================================================= -->
		<div class="blog">
			<div class="container">
          		<?php if($blogs) :?>
					<?php $count = 'left'; ?>
					<!-- Post Starts -->
					<?php foreach($blogs as $blog):?>
					 	<?php if($count ==  'left'): ?>

							<div class="row no-gutter-8 no-gutter-4 post">
								<div class="col-lg-8 col-md-8">
									<div class="picture">
										<img src="<?=base_url()?>assets/uploads/blogs/<?=$blog->cover_image ?>" class="img-responsive" alt="">
										<div class="date left">
											<div class="day"><?php echo date('d M', strtotime($blog->created_date))?>
											</div>
										</div>
									</div>
								</div>

								<div class="col-lg-4 col-md-4">
									<div class="details">
										<h1><?=$blog->title ?></h1>
										<div class="description"><?= substr(strip_tags($blog->contents), 0, 250);?>...</div>
										<div class="button-meta clearfix">
											<div class="meta"><span class="user"><a href=""> <a href=""><i class="fa fa-user"></i> Guruji</a></span><span class="comment"><a href="#"><i class="fa fa-eye"></i><?=$blog->impressions ?> Views</a></span></div>
											<div class="button"><a href="<?= base_url().'blog/'.$blog->slug;?>" class="fill" >Read More</a></div>
										</div>
									</div>
								</div>
								<?php $count = 'right'; ?>
							</div>
						<?php else :?>

							<div class="row no-gutter-8 no-gutter-4 post">
								<div class="col-lg-8 col-md-8 right pull-right">
									<div class="picture">
										<img src="<?=base_url()?>assets/uploads/blogs/<?=$blog->cover_image ?>" class="img-responsive" alt="">
										<div class="date left">
											<div class="day"><?php echo date('d M', strtotime($blog->created_date))?>
											</div>
										</div>
									</div>

								</div>
								<div class="col-lg-4 col-md-4">
									<div class="details">
										<h1><?=$blog->title ?></h1>
										<div class="description"><?= substr(strip_tags($blog->contents), 0, 300);?>...</div>
										<div class="button-meta clearfix">
											<div class="meta"><span class="user"><a href=""> <a href=""><i class="fa fa-user"></i> Guruji</a></span><span class="comment"><a href="#"><i class="fa fa-eye"></i><?=$blog->impressions ?> Views</a></span></div>
											<div class="button" ><a href="<?= base_url().'blog/'.$blog->slug;?>" class="fill">Read More</a></div>
										</div>
									</div>
								</div>	
								<?php $count = 'left'; ?>
							</div>
						<?php endif; ?>

					<?php endforeach;?>

				<?php else :?>
					<br>
					<h3 align="center"><i class="fa fa-frown-o" aria-hidden="true"></i> Oh Snap! No News or Events</h3>
					<h4 align="center">We shall update soon</h4>
					<br>
				<?php endif; ?>	
						</div>
							<div class="col-lg-12">
								<div class="big-button"><a href="" class="blue-fill">More News</a></div>
							</div>					
						</div>
		
				<!-- Post Ends -->
			</div>
		</div>

		<!-- /. BLOG CONTENTS ENDS
			========================================================================= -->
