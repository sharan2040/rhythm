		<!-- CLASSES CONTENTS STARTS
			========================================================================= -->
		<div class="classes">
			<div class="container">
          	<?php if($news) :?>
				<div class="row intro">
					<div class="col-lg-12">
						<h1>News & Events</h1>
						<h2>Etiam rhoncus. Maecenas tempus</h2>
					</div>
				</div>
				<!-- Classe Block Starts -->
				<?php foreach($news as $events):?>
					<div class="row block">
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="picture"><img src="<?=base_url()?>assets/uploads/news_and_events/<?=$events->cover_image ?>" width="100%" ></div>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
							<div class="info">
								<div class="dept"><span class="name"><?=$events->title ?></span></div>
								<div class="description"><?= substr(strip_tags($events->contents), 0, 250);?>...</div>
								<div class="button"><a href="<?= base_url().'news/'.$events->slug;?>" class="fill">Read More</a> <span class="price">$ 87 / month</span></div>
								<ul class="social-icons">
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="more-info">
								<div class="complexity">
									<h2>the complexity:</h2>
									<ul class="stars">
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star-o"></i></li>
										<li><i class="fa fa-star-o"></i></li>
									</ul>
								</div>
								<div class="persons">
									<h2>Number of Persons</h2>
									<div class="number">6</div>
								</div>
								<div class="occupation">
									<h2>The Occupation</h2>
									<div class="duration"><span>90 min</span></div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach;?>
				<!-- Classe Block Ends -->

			<?php else :?>
				<br>
				<h3 align="center"><i class="fa fa-frown-o" aria-hidden="true"></i> Oh Snap! No Articles to Display</h3>
				<h4 align="center">We shall update soon</h4>
				<br>
			<?php endif; ?>	

				<div class="row">
					<div class="col-lg-12">
						<div class="big-button"><a href="" class="blue-fill"><strong>Show all</strong> Classes</a></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /. CLASSES CONTENTS ENDS
			========================================================================= -->