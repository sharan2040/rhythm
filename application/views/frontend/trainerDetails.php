		<!-- CLASSES CONTENTS STARTS
			========================================================================= -->
		<div class="trainer-details">
			<div class="container">
				<div class="row intro">
					<div class="col-lg-12">
						<h1>bruce luis</h1>
						<h2>boxing rhoncus. Maecenas tempus</h2>
					</div>
				</div>
				<!-- Pictures Carousel Starts -->
				<div class="row">
					<div class="col-lg-12">
						<div class="trainer-carousel">
							<div class="picture"><img src="images/trainer/detail/1.jpg" class="img-responsive" alt=""></div>
							<div class="picture"><img src="images/trainer/detail/2.jpg" class="img-responsive" alt=""></div>
						</div>
					</div>
				</div>
				<!-- Pictures Carousel Ends -->
				<!-- Description Starts -->
				<div class="row">
					<div class="col-lg-6 col-md-6 description">Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. <br><br>
						Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
					</div>
					<div class="col-lg-6 col-md-6 description">Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. <br><br>
						Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
					</div>
				</div>
				<!-- Description Ends -->
				<!-- Three Columns Starts -->
				<div class="row three-columns">
					<!-- Col Starts -->
					<div class="col-lg-4">
						<div class="block medium-grey-bg-2">
							<div class="icon"><i class="fa fa-money"></i></div>
							<ul class="details">
								<li>Price for month: 87$ unlimited</li>
								<li>One-time visit: 15$ per lesson</li>
								<li>Class with coach: 30$ per lesson</li>
							</ul>
						</div>
					</div>
					<!-- Col Starts -->
					<!-- Col Starts -->
					<div class="col-lg-4">
						<div class="block blue-bg">
							<div class="icon"><i class="fa fa-clock-o"></i></div>
							<ul class="details">
								<li>monday - wednestday: 12:00, 18:30</li>
								<li>tuesday - thursday: 11:30, 19:20</li>
								<li>friday - Sunday: no lessons</li>
							</ul>
						</div>
					</div>
					<!-- Col Starts -->
					<!-- Col Starts -->
					<div class="col-lg-4">
						<div class="block medium-grey-bg-2">
							<div class="icon"><i class="fa fa-hand-paper-o"></i></div>
							<ul class="details">
								<li>the complexity:</li>
								<li>number of persons: 12</li>
								<li>the occupation: 120 min</li>
							</ul>
						</div>
					</div>
					<!-- Col Starts -->
				</div>
				<!-- Three Columns Ends -->
			</div>
			<!-- OUR PHOTOS STARTS
				========================================================================= -->
			<div class="our-photos">
				<div class="medium-grey-bg heading-strip">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
								<h1>Our Photos</h1>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="button"><a href="" class="white">View All</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row no-gutter-3">
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/1.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/2.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/3.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/4.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/5.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/6.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/7.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
						<!-- Picture Starts -->
						<div class="col-lg-3 col-md-4 col-sm-6 block">
							<div class="picture">
								<img src="images/photos/8.jpg" class="img-responsive" alt="">
								<!-- Picture Overlay Starts -->
								<div class="portfolio-overlay">
									<div class="icons">
										<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-search"></i></a></span></div>
									</div>
								</div>
								<!-- Picture Overlay Ends -->
							</div>
						</div>
						<!-- Picture Ends -->
					</div>
				</div>
			</div>
			<!-- /. OUR PHOTOS ENDS
				========================================================================= -->
		</div>
		<!-- /. CLASSES CONTENTS ENDS
			========================================================================= -->