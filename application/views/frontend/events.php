		<!-- EVENTS CONTENTS STARTS
			========================================================================= -->
		<div class="events">
			<!-- INTRO STARTS
				========================================================================= -->
			<div class="container intro">
				<div class="row">
					<div class="col-lg-12">
						<h1>Events</h1>
						<h2>Etiam rhoncus. Maecenas tempus</h2>
					</div>
				</div>
			</div>
			<!-- /. INTRO ENDS
				========================================================================= -->
   
			<!-- INTRO STARTS
				========================================================================= -->
			<div class="container intro">
				<div class="row">
					<div class="col-lg-8 col-md-8">
						<div class="description">Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.<br><br> 
							Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br><br>
							Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. 
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="picture">
							<img src="images/schedule/1.jpg" class="img-responsive" alt="">
							<!-- Picture Overlay Starts -->
							<div class="portfolio-overlay">
								<div class="icons">
									<div><span class="icon"><a class="image-popup-vertical-fit" href="images/photos/1.jpg" title="Caption will be here"><i class="fa fa-play"></i></a></span></div>
								</div>
							</div>
							<!-- Picture Overlay Ends -->
						</div>
						<div class="description-2">Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. </div>
					</div>
				</div>
			</div>
			<!-- /. INTRO ENDS
				========================================================================= -->
		</div>
		<!-- /. EVENTS CONTENTS ENDS
			========================================================================= -->