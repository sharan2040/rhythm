		<!-- TRAINER CONTENTS STARTS
			========================================================================= -->
		<div class="trainer">
			<div class="container">
				
	          	<?php if($trainers) :?>
					<?php foreach($trainers as $trainer):?>

					<!-- Trainer Block Starts -->
					<div class="row block">
						<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
							<div class="picture"><img src="<?= base_url().'assets/uploads/trainers/'.$trainer->profile_image?>" class="img-responsive" alt="" ></div>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
							<div class="info">
								<div class="dept"><?=$trainer->trainer_name ?></span></div>
								<div class="description"><?=$trainer->bio ?></div>
								<ul class="social-icons">
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
									<li><a href=""><i class="fa fa-phone"></i></a></li>
								</ul>
							</div>
						</div>
						
					</div>
					<!-- Trainer Block Ends -->

					<?php endforeach;?>
				<?php else :?>
					<div class="row block">
						<h3 align="center"><i class="fa fa-frown-o" aria-hidden="true"></i> Oh Snap! No News or Events</h3>
						<h4 align="center">We shall update soon</h4>					
					</div>
				<?php endif; ?>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="big-button"><a href="" class="blue-fill"><strong>Show all</strong> Instructors</a></div>
					</div>
				</div>
			</div>
		</div>
		<!-- /. TRAINER CONTENTS ENDS
			========================================================================= -->

