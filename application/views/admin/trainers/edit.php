<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Programmes</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<img src="<?= base_url().'assets/uploads/trainers/'.$trainers[0]->profile_image ?>" width="100%">
					</div>
				</div>

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/trainers/update" style="display:inline;" enctype="multipart/form-data">

							<input type="hidden" name="trainerId" value="<?= $trainers[0]->trainer_id ?>">

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Name</label>
								<input type="text" class="form-control" name="trainerName" value="<?= $trainers[0]->trainer_name ?>" required >
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Profile Image</label>
								<input type="file" class="form-control" name="profileImage"  >
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Biodata</label>
								<textarea name="bio" id="trainersText" required><?=$trainers[0]->bio ?></textarea> 
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Facebook URL</label>
								<input type="text" class="form-control" name="facebookUrl" value="<?=$trainers[0]->facebook_url ?>" >
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Twitter URL</label>
								<input type="text" class="form-control" name="twitterUrl" value="<?=$trainers[0]->twitter_url ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Instagram URL</label>
								<input type="text" class="form-control" name="instagramUrl" value="<?=$trainers[0]->instagram_url ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">E-Mail Address</label>
								<input type="text" class="form-control" name="mailAddress" value="<?=$trainers[0]->mail_address ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Mobile Number</label>
								<input type="text" class="form-control" name="mobileNumber" value="<?=$trainers[0]->mobile_number ?>">
								<small class="text-help"></small>
							</fieldset>

							<button type="submit" class="btn btn-primary">Update</button>
 						</form> 

						<form method="POST" action="<?php echo base_url()?>admin/trainers/delete" style="display:inline;">
							<input type="hidden" name="trainerId" value="<?=$trainers[0]->trainer_id ?>">
 							<button type="submit" class="btn btn-danger">Delete</button>
 						</form>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#trainersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



