   <!-- Charts CSS -->
   <link rel="stylesheet" href="<?= base_url()?>assets/admin/examples/css/morris.min.css">


   <!-- Content -->
   <div class="layout-content" data-scrollable>
    <div class="container-fluid">     

      <!-- Breadcrumb -->
      <ol class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li><a href="#">Dashboard</a></li>
        <li class="active">Data</li>
      </ol>

      <!-- Row -->
      <div class="row">

        <!-- Column -->
        <div class="col-md-4">
          <div class="card">
            <div class="card-block center">
              <h5 class="card-title">Volume Trend</h5>
              <p class="card-subtitle">340 Highest</p>
              <div id="line1" style="height: 130px; margin:0 -10px;"></div>
              <a href="#" class="btn btn-primary btn-rounded-deep btn-sm">More Details</a>
            </div>
          </div>
        </div>
        <!-- // END Column -->

        <!-- Column -->
        <div class="col-md-4">
          <div class="card">
            <div class="card-button-wrapper">
              <a href="#" class="card-button"><i class="material-icons">settings</i></a>
            </div>
            <div class="card-block center">
              <h5 class="card-title">Receipts</h5>
              <p class="card-subtitle">482 Prescribed</p>
              <div id="bar2" style="height: 130px; margin:0 -10px;"></div>
              <a href="#" class="btn btn-primary btn-rounded-deep btn-sm">View Report</a>
            </div>
          </div>
        </div>
        <!-- // END Column -->

        <!-- Column -->
        <div class="col-md-4">
          <div class="card">
            <div class="card-header bg-white center">
              <h5 class="card-title">Top Member</h5>
              <p class="card-subtitle m-b-0">Adrian Demian</p>
            </div>
            <table class="table table-sm m-b-0">
              <tr>
                <td><i class="material-icons text-primary">person</i> <span class="icon-text"><a href="#">Adrian Demian</a></span></td>
                <td class="right">
                  <div class="label label-success">49</div>
                </td>
                <td class="right" width="1"><a href="#" class="btn btn-xs btn-white"><i class="material-icons md-18">chevron_right</i></a></td>
              </tr>
              <tr>
                <td class="text-muted"><i class="material-icons text-muted">person</i> <span class="icon-text">Michelle Smith</span></td>
                <td class="right">
                  <div class="label label-default">24</div>
                </td>
                <td class="right" width="1"><a href="#" class="btn btn-xs btn-white"><i class="material-icons md-18">chevron_right</i></a></td>
              </tr>

              <tr>
                <td class="text-muted"><i class="material-icons text-muted">person</i> <span class="icon-text">Jonny Clint</span></td>
                <td class="right">
                  <div class="label label-default">16</div>
                </td>
                <td class="right" width="1"><a href="#" class="btn btn-xs btn-white"><i class="material-icons md-18">chevron_right</i></a></td>
              </tr>
              <tr>
                <td class="text-muted"><i class="material-icons text-muted">person</i> <span class="icon-text">Andrew Brain</span></td>
                <td class="right">
                  <div class="label label-default">13</div>
                </td>
                <td class="right" width="1"><a href="#" class="btn btn-xs btn-white"><i class="material-icons md-18">chevron_right</i></a></td>
              </tr>
              <tr>
                <td class="text-muted"><i class="material-icons text-muted">person</i> <span class="icon-text">Bill Carter</span></td>
                <td class="right">
                  <div class="label label-default">5</div>
                </td>
                <td class="right"><a href="#" class="btn btn-xs btn-white"><i class="material-icons md-18">chevron_right</i></a></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- // END Column -->

      </div>
      <!-- // END Row -->

      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="card-block">
              <div id="donut" style="width: 100%; height:230px;"></div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-block">
              <div id="bar" style="width: 100%; height:230px;"></div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- Theme Colors -->
  <script src="<?=base_url()?>assets/admin/assets/js/colors.js"></script>

  <!-- Charts JS -->
  <script src="<?=base_url()?>assets/admin/assets/vendor/raphael-min.js"></script>
  <script src="<?=base_url()?>assets/admin/assets/vendor/morris.min.js"></script>

  <!-- Initialize Charts -->

  <script>
  (function ($) {

    if ($('#donut').length) {
      new Morris.Donut({
        element: 'donut',
        data: [
        { label: "New/Unattended Enquiries", value: 3 },
        { label: "Rejected Enquiries", value: 5 },
        { label: "Attended Enquiries", value:7 }
        ],
        colors: [ colors[ 'chart-primary' ],  colors[ 'chart-secondary' ],  colors[ 'chart-third' ]],
        resize: true
      });
    }

    if ($('#bar2').length) {
    new Morris.Bar({
      element: 'bar2',
      data: [
        { y: '2006', a: 100 },
        { y: '2007', a: 75 },
        { y: '2008', a: 50 },
        { y: '2009', a: 75 },
        { y: '2010', a: 50 },
        { y: '2011', a: 75 },
        { y: '2012', a: 100 },
        { y: '2013', a: 200 },
        { y: '2014', a: 300 },
        { y: '2015', a: 260 },
        { y: '2016', a: 40 }
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Sales'],
      barColors: [ colors[ 'chart-primary' ] ],
      barShape: 'soft',
      xLabelMargin: 10,
      resize: true
    });
  }

  if ($('#bar').length) {
    new Morris.Bar({
      element: 'bar',
      data: [
        { y: 'January', a: 100 },
        { y: 'February', a: 75 },
        { y: 'March', a: 50 },
        { y: 'April', a: 75 },
        { y: 'May', a: 90 },
        { y: 'June', a: 50 },
        { y: 'July', a: 75 },
        { y: 'August', a: 100 },
        { y: 'September', a: 200 },
        { y: 'October', a: 300 },
        { y: 'November', a: 260 },
        { y: 'December', a: 40}
      ],
      xkey: 'y',
      ykeys: ['a'],
      labels: ['Sales'],
      barColors: [ colors[ 'chart-primary' ] ],
      barShape: 'soft',
      xLabelMargin: 10,
      resize: true
    });
  }

  if ($('#line1').length) {
    new Morris.Line({
      element: 'line1',
      data: [
        { y: '2008', a: 150, b:50 },
        { y: '2009', a: 75, b:90 },
        { y: '2010', a: 200, b:120 },
        { y: '2011', a: 75, b:340 },
        { y: '2012', a: 130, b:60 }
      ],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['New', 'Resolved'],
      lineColors: [ colors[ 'chart-primary' ], colors[ 'chart-secondary' ]],
      resize: true
    });
  }

  }(jQuery));
 </script>


