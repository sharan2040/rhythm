<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<!-- For Displaying Notification -->
		<?php if( $this->session->flashdata('settingsUpdated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Settings Updated.
			</div>
		<?php endif;?>


		<ol class="breadcrumb">
			<li><a href="<?php echo base_url()?>admin/">Home</a></li>
			<li class="active">Settings</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/settings/updateSettings">
 							<button type="submit" class="btn btn-primary pull-right">Update</button><div class="clearfix"></div>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Contact Email</label>
								<input type="text" class="form-control" value="<?= $settings[0]->contact_email;?>" name="contactEmail">
								<small class="text-help">The mail to which website enquiries would be sent | Also Displayed in Contact page, Footer & Header</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Contact Number</label>
								<input type="text" class="form-control" value="<?= $settings[0]->contact_phone;?>" name="contactNumber">
								<small class="text-help">Displayed in Contact page, Footer & Header</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Contact Fax</label>
								<input type="text" class="form-control" value="<?= $settings[0]->contact_fax;?>" name="contactFax">
								<small class="text-help">Displayed in Contact page and Footer</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Company Address</label>
								<input type="text" class="form-control" value="<?= $settings[0]->company_address;?>" name="companyAddress">
								<small class="text-help">Displayed in Contact page and Footer</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Working Hours</label>
								<input type="text" class="form-control" value="<?= $settings[0]->working_hours;?>" name="workingHours">
								<small class="text-help">Displayed in Contact page and Footer</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Latitude</label>
								<input type="text" class="form-control" value="<?= $settings[0]->latitude;?>" name="latitude">
								<small class="text-help">Location coordinate</small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Longitude</label>
								<input type="text" class="form-control" value="<?= $settings[0]->longitude;?>" name="longitude">
								<small class="text-help">Location coordinate</small>
							</fieldset>

					</div>
				</div>

				<div class="card">
					<div class="card-block">
						<h5>Social Links</h5>
							<fieldset class="form-group">
								<label for="exampleInputEmail1">Facebook</label>
								<input type="text" class="form-control" value="<?= $settings[0]->facebook_link;?>" name="facebook">
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Google+</label>
								<input type="text" class="form-control" value="<?= $settings[0]->google_link;?>" name="google">
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Twitter</label>
								<input type="text" class="form-control" value="<?= $settings[0]->twitter_link;?>" name="twitter">
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">LinkedIn</label>
								<input type="text" class="form-control" value="<?= $settings[0]->linkedin_link;?>" name="linkedin">
							</fieldset>

						</form>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#careersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



