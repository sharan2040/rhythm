<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">


<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<!-- For Displaying Notification for Creating Programme-->
		<?php if( $this->session->flashdata('eventCreated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Event Created.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification for deleting a Programme-->
		<?php if( $this->session->flashdata('eventDeleted') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Event Deleted.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification to update Programme-->
		<?php if( $this->session->flashdata('eventUpdated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Event  Updated.
			</div>
		<?php endif;?>


		<ol class="breadcrumb">
			<li><a href="<?php echo base_url()?>admin/">Home</a></li>
			<li class="active">Events </li>
		</ol>

		<div class="row">
			<div class="col-xl-12">

				<a href="<?php echo base_url()?>admin/events/add"><button type="button" class="btn btn-primary pull-right">Add New</button></a><div class="clearfix"></div><br>

				<div class="card">
					<div class="card-block">
						<h5>List Of Events</h5>
						<table id="endquiryTable" class="table table-striped table-hover table-sm">
							<thead>
								<tr>
									<th>Event ID</th>
									<th>Title</th>
									<th>Cover Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($events as $event) : ?>
								<tr>
									<td required="required"><?= $event->event_id ?></td>
									<td required="required"><?= $event->title ?></td>
									<td><img src="<?= base_url().'assets/uploads/events/'.$event->cover_image ?>" width="150"></td>
									<td required="required"><a href="<?php echo base_url()?>admin/events/edit/<?= $event->event_id ?>">Edit/ Delete</a></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#careersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



