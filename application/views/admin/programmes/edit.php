<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li class="active">Programmes</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<img src="<?= base_url().'assets/uploads/programs/'.$programmes[0]->cover_image ?>" width="100%">
					</div>
				</div>

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/programmes/update" style="display:inline;" enctype="multipart/form-data">

							<input type="hidden" name="mainProgrammeId" value="<?=$programmes[0]->main_programme_id ?>">

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Title</label>
								<input type="text" class="form-control" name="title" value="<?=$programmes[0]->title ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Main Caption</label>
								<input type="text" class="form-control" name="mainCaption" value="<?=$programmes[0]->main_caption ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Sub Caption</label>
								<input type="text" class="form-control" name="subCaption" value="<?=$programmes[0]->sub_caption ?>">
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Cover Image</label>
								<input type="file" class="form-control" name="coverImage" >
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Description</label>
								<textarea name="description" id="programmesText"><?=$programmes[0]->description ?></textarea> 
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Class Timings</label>
								<textarea name="classTimings" id="timingsText"><?=$programmes[0]->class_timings ?></textarea> 
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Packages & Rates</label>
								<textarea name="packagesAndRates" id="packagesText"><?=$programmes[0]->packages_and_rates ?></textarea> 
								<small class="text-help"></small>
							</fieldset>							

						<button type="submit" class="btn btn-primary">Update</button>
 						</form>
						<form method="POST" action="<?php echo base_url()?>admin/programmes/delete" style="display:inline;">
							<input type="hidden" name="mainProgrammeId" value="<?=$programmes[0]->main_programme_id ?>">
 							<button type="submit" class="btn btn-danger">Delete</button>
 						</form>
					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#programmesText').APSummernote();
	 $('#timingsText').APSummernote();
	 $('#packagesText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



