<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">




<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>admin/dashboard">Home</a></li>
			<li class="active">Testimonials</li>
		</ol>

		
		<div class="row">
			<div class="col-xl-12">

				<div class="card">
					<div class="card-block">
						<form method="POST" action="<?php echo base_url()?>admin/testimonials/update" style="display:inline;" enctype="multipart/form-data">

							<input type="hidden" name="testimonialId" value="<?= $testimonials[0]->testimonial_id ?>">

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Student Name</label>
								<input type="text" class="form-control" name="studentName" value="<?= $testimonials[0]->student_name ?>" required >
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<?php if($programmes) :?>
									<select class="form-control" name="mainProgramId">
										<?php foreach($programmes as $program) :?>	
											<option value="<?=$program->main_programme_id ?>" <?php if($program->main_programme_id == $testimonials[0]->main_programme_id) echo "selected";?>><?=$program->title ?></option>
										<?php endforeach ;?>
									</select>
								<?php endif ;?>
							</fieldset>	

							<fieldset class="form-group">
								<select class="form-control" name="currentStatus">
									<option value="0" <?php if($testimonials[0]->status == '0') echo "Selected"?>>Inactive</option>
									<option value="1" <?php if($testimonials[0]->status == '1') echo "Selected"?>>Active</option>								
								</select>
								<small class="text-help"></small>
							</fieldset>

							<fieldset class="form-group">
								<label for="exampleInputEmail1">Content</label>
								<textarea name="content" id="blogText" class="form-control" required="required"><?= $testimonials[0]->content ?></textarea>
								<small class="text-help"></small>
							</fieldset>

							<button type="submit" class="btn btn-primary">Update</button>
 						</form> 

						<form method="POST" action="<?php echo base_url()?>admin/testimonials/delete" style="display:inline;">
							<input type="hidden" name="testimonialId" value="<?=$testimonials[0]->testimonial_id ?>">
 							<button type="submit" class="btn btn-danger">Delete</button>
 						</form>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#blogText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>



