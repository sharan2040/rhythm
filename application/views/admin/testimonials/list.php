<!-- Summernote WYSIWYG -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/summernote.min.css">

<!-- Datepicker -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-datepicker.min.css">

<!-- Touchspin -->
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?=base_url()?>assets/admin/examples/css/datatables.min.css">


<!-- Content -->
<div class="layout-content" data-scrollable>
	<div class="container-fluid">

		<!-- For Displaying Notification for Creating Programme-->
		<?php if( $this->session->flashdata('testimonialCreated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Testimonial Created.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification for deleting a Programme-->
		<?php if( $this->session->flashdata('testimonialDeleted') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Testimonial Deleted.
			</div>
		<?php endif;?>

		<!-- For Displaying Notification to update Programme-->
		<?php if( $this->session->flashdata('testimonialUpdated') == true ): ?>
			<div class="alert alert-success alert-fixed">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Success!</strong> | <i class="fa fa-check-square-o" aria-hidden="true"></i> Testimonial  Updated.
			</div>
		<?php endif;?>


		<ol class="breadcrumb">
			<li><a href="<?=base_url()?>admin/dashboard">Home</a></li>
			<li class="active">Testimonials </li>
		</ol>

		<div class="row">
			<div class="col-xl-12">

				<a href="<?php echo base_url()?>admin/testimonials/add"><button type="button" class="btn btn-primary pull-right">Add New</button></a><div class="clearfix"></div><br>

				<div class="card">
					<div class="card-block">
						<h5>List Of Testimonials</h5>
						<table id="endquiryTable" class="table table-striped table-hover table-sm">

							<thead>
								<tr>
									<th>Testimonial Id</th>
								 	<th>Program </th>
									<th>Student Name</th>
								 	<th>Status</th>
									<th>Action</th>
								</tr>
							</thead> 

							<tbody>
								<?php foreach ($testimonials as $testimonial) : ?>
								<tr>
									<td><?= $testimonial->testimonial_id ?></td>
									<td><?= $testimonial->title ?></td> 
									<td><?= $testimonial->student_name ?></td>
									<td>
					                <?php if( $testimonial->status == 0 ):?>
					                <span class="label label-danger">Inactive</span>
					                <?php elseif( $testimonial->status == 1 ):?>
					                <span class="label label-success">Active</span>
					                <?php endif;?>  									
						            </td>
									<td><a href="<?php echo base_url()?>admin/testimonials/edit/<?= $testimonial->testimonial_id ?>">Edit/ Delete</a></td>
								</tr>
								<?php endforeach; ?>
							</tbody> 

						</table>
						<div class="clearfix"></div>
					</div>
				</div>


			</div>
		</div>	
	</div>
</div>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/datatables-bootstrap3.min.js"></script>


<!-- Bootstrap -->
<script src="<?=base_url()?>assets/admin/assets/vendor/tether.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap.min.js"></script>

<!-- AdminPlus -->
<script src="<?=base_url()?>assets/admin/assets/vendor/adminplus.js"></script>

<!-- App JS -->
<script src="<?=base_url()?>assets/admin/assets/js/main.min.js"></script>

<!-- Vendor JS -->
<script src="<?=base_url()?>assets/admin/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/bootstrap-timepicker.js"></script>
<script src="<?=base_url()?>assets/admin/assets/vendor/summernote.min.js"></script>

<!-- Init -->
<script src="<?=base_url()?>assets/admin/examples/js/date-time.js"></script>
<script src="<?=base_url()?>assets/admin/examples/js/touchspin.js"></script>

<script>
	(function ($) {
	/**
	 * jQuery plugin wrapper for compatibility
	 */
	 $.fn.APSummernote = function () {
	 	if (! this.length) return;
	 	if (typeof $.fn.summernote != 'undefined') {
	 		this.summernote({
	 			height: 350,
	 			popover: {
	 				image: [],
	 				link: [],
	 				air: []
	 			}
	 		});
	 	}
	 };

	 $('#careersText').APSummernote();
	 $('.datepicker').plusDatePicker();

	 $('#enquiryTable').DataTable();

	}(jQuery));
</script>




