<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title><?= $title?> | Rhythm Yoga</title>
		<!-- All Stylesheets -->
		<link href="<?= base_url() ?>assets/frontend/css/all-stylesheets.css" rel="stylesheet">
		<!-- ALL COLORED STYLESHEETS -->
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/blue.css" title="default">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/green.css" title="green">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/red.css" title="red">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/orange.css" title="orange">
		<link rel="alternate stylesheet" type="text/css" href="<?= base_url() ?>assets/frontend/css/colors/pink.css" title="pink">

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- BLOG BANNER STARTS -->
		<div class="inner-banner parallax-2">
			<div class="header">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-md-7 col-sm-6">
							<h1><span><?= $title?></span></h1>
							<ol class="breadcrumb">
								<li><a href="<?= base_url()?>" class="external">Home</a></li>
								
								<li class="active">Blog</li>
							</ol>
						</div>
						<div class="col-lg-4 col-md-5 col-sm-6">
							<img src="<?= base_url()?>/assets/frontend/images/logo.png" height="80" />
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- BLOG BANNER ENDS -->
		<!-- NAVIGATION STARTS
			========================================================================= -->
		<nav id="navigation" class="navbar navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html"><img src="<?= base_url() ?>assets/frontend/images/logo-menu.png" height="70" alt="" ></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav pull-right">
						
						<li><a href="#" class="external">Home</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url()?>contact" class="external">Contact us</a></li>
								<li><a href="<?= base_url()?>about-institute" class="external">About Institute</a></li>
								<li><a href="#" class="external">Guruji Sanjeev</a></li>
								<li><a href="#" class="external">Our Centres</a></li>
							</ul>
						</li>
						<li class="dropdown <?php if($this->uri->segment(1) == 'programmes'){ echo 'active'; }?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programs <span class="caret"></span></a>
							<ul class="dropdown-menu">
							<?php foreach($programmes as $programme) :?>
								<li><a href="<?= base_url().'programmes/'.$programme->slug?>" class="external"><?= $programme->title?></a></li>
							<?php endforeach;?>
							</ul>
						</li>
						<li class="<?php if($this->uri->segment(1) == 'trainers'){ echo 'active'; }?>"><a href="<?= base_url()?>trainers" class="external">Trainers</a></li>
						<li><a href="#" class="external">News & Events</a></li>
						<li class="dropdown">
							<a href="<?= base_url()?>testimonials" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gallery <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#" class="external">Image Gallery</a></li>
								<li><a href="#" class="external">Video Gallery</a></li>
							</ul>
						</li>
						<li><a href="#" class="external">Stories</a></li>
						<li class="<?php if($this->uri->segment(1) == 'blog'){ echo 'active'; }?>"><a href="<?= base_url()?>blogs" class="external">Blog</a></li>
						
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
		<!-- /. NAVIGATION ENDS
