<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Events_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function listEvents($fields = false, $conditions = false){

		if($fields != false){
			$this->db->select($fields);
		}

		$this->db->from('events');

		 if($conditions != false){
		 	$this->db->where($conditions);
		 }

		 $this->db->order_by('event_id', 'desc');
		 $query = $this->db->get();
		 return $query->result();
	}

	public function createEvent($data){
		if($data){
			$this->db->insert('events', $data);
			return true;
		}
	}

	public function updateEvent($fields = false, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('events', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	
	}

	public function deleteEvent($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('events', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
	}
}
