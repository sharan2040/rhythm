<?php

/**
 * Description    : Model to Manage Contact mail
 * Created        : 22-01-16
 */

class Email_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }



    public function listTemplates($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('mail_templates');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $query = $this->db->get();
        return  $query->result();
    }




}
