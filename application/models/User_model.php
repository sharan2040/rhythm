<?php

/**
 * Author         : Toobler
 * Company        : Toobler Technologies
 * Project        : SelfiePleasure
 * Description    : Model to Manage Users
 * Created        : 22-01-16
 */

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

     /**
     * @uses    users full data
     */
    public function userData($fields = FALSE, $conditions = FALSE)
    {
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('users');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $query = $this->db->get();
        return  $query->result();
    }


    public function userUpdate($fields = FALSE, $conditions)
    {
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('sp_users', $fields); 
                return TRUE;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

} //Model Ends Here