<?php 
/**
 * Description    : Model to Manage Reminders
 * Created        : 06-06-16
 */

class Reminders_model extends CI_model{
    public function __construct()
    {
        parent::__construct();
    }

	public function createReminder($data){
		if($data){
			$this->db->insert('reminders', $data);
			return TRUE;
		}
	}

	public function listReminder($fields = FALSE, $conditions = FALSE){
        if($fields != FALSE){
            $this->db->select($fields);
        }
        $this->db->from('reminders');

        if ($conditions != FALSE) {
            $this->db->where($conditions);
        }

        $this->db->order_by('reminder_id', 'desc');

        $query = $this->db->get();
        return  $query->result();
	}

   public function updateReminder($fields = FALSE, $conditions)
    {
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('reminders', $fields); 
                return TRUE;
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    } 

	public function deleteReminder($condition = FALSE){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('reminders', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
    }
}