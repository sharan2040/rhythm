<?php 

/**
* 
*/
class Programmes_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}


	public function listProgrammes($fields = false, $conditions = false){
		if($fields != false){
			$this->db->select($fields);
		}
		$this->db->from('main_programmes');

		if($conditions != false){
			$this->db->where($conditions);
		}

		$this->db->order_by('main_programme_id', 'asc');

		$query = $this->db->get();
		return $query->result();
	}

	public function createProgramme($data){
		if($data){
		$this->db->insert('main_programmes', $data);
		return TRUE;
		}
	}

	public function updateProgramme($fields = false, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('main_programmes', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	

	}
	
	public function deleteProgramme($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('main_programmes', $condition);
                $this->db->delete('testimonials',$condition);
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
	}
}