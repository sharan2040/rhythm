<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Contactus_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function createContact($data){
		if($data){
			$this->db->insert('contact_us', $data);
			return TRUE;
		}
	}
}