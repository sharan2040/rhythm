<?php

/**
* 
*/
class Trainers_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function listTrainers($fields = false, $conditions = false, $limit = false){
		if($fields != false){
			$this->db->select($fields);
		} 

		$this->db->from('trainers');

		if($conditions != false){
			$this->db->where($conditions);
		}

		if($limit != false){
			$this->db->where($limit);
		}

		$this->db->order_by('trainer_id', 'desc'); 

		$query = $this->db->get();
		return $query->result();
	}

	public function createtrainer($data){
		if($data){
			$this->db->insert('trainers', $data);
			return true ;
		}
	}

	public function updateTrainer($fields = false, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('trainers', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	

	}

	public function deleteTrainer($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('trainers', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
	}
}