<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Testimonials_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function createTestimonial($data){
		if($data){
			$this->db->insert('testimonials', $data);
			return TRUE;
		}
	}

	public function listTestimonials($fields =FALSE, $conditions = FALSE){
		if($fields != FALSE){
			$this->db->select($fields);
		}

		$this->db->from('testimonials');

		if($conditions != FALSE){
			$this->db->where($conditions);
		}

		$this->db->order_by('testimonial_id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function updateTestimonial($fields = false, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('testimonials', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	
  	}

  	public function deleteTestimonial($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('testimonials', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
  	}

    /**
     * @uses 		To retrieve list of testimonials from two tables using join operation
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */
  	public function listing(){
		
		$this->db->select('testimonial.testimonial_id, testimonial.student_name,testimonial.status, programmes.main_programme_id, programmes.title');
		$this->db->from('testimonials as testimonial');
		$this->db->join('main_programmes as programmes', 'testimonial.main_programme_id = programmes.main_programme_id','left');
		$this->db->order_by('testimonial.testimonial_id', 'desc');

		$query = $this->db->get();
		return $query->result();  
	}
}
