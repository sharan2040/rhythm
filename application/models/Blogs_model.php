<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Blogs_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function listBlogs($fields = false, $conditions = false){

		if($fields != false){
			$this->db->select($fields);
		}

		$this->db->from('blogs');

		 if($conditions != false){
		 	$this->db->where($conditions);
		 }

		 $this->db->order_by('blog_id', 'desc');
		 $query = $this->db->get();
		 return $query->result();
	}

	public function createBlog($data){
		if($data){
			$this->db->insert('blogs', $data);
			return true;
		}
	}

	public function updateBlog($fields = false, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('blogs', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	
	}

	public function deleteBlog($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('blogs', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
	}

	//Increment the Impressions by one for each Load
	public function incrementImpressions($id = null){
		
		if($id != null){
            $this->db->where('blog_id', $id);
            $this->db->set('impressions', 'impressions+1', FALSE);
			$this->db->update('blogs'); 
		}

	}
}
