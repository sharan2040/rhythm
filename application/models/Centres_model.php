<?php

/**
* 
*/
class Centres_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function createCentre($data){
		if($data){
			$this->db->insert('centres', $data);
			return true;
		}
	}

	public function listCentres($fields = false, $conditions = false){

		if($fields != false){
			$this->db->select($fields);
		}

		$this->db->from('centres');

		if ($conditions != false) {
			$this->db->where($conditions);
		}

		$this->db->order_by('centre_id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function updateCentre($fields = false, $conditions){
        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('centres', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	
	}

	public function deleteCentre($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('centres', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
	}
}
