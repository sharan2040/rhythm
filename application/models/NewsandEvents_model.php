<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class NewsandEvents_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function createNews($data){
		if($data){
			$this->db->insert('news_and_events', $data);
			return TRUE;
		}
	}

	public function listNews($fields =FALSE, $conditions = FALSE, $limit = FALSE){

		if($fields != FALSE){
			$this->db->select($fields);
		}

		$this->db->from('news_and_events');

		if($conditions != FALSE){
			$this->db->where($conditions);
		}

    if($limit != FALSE){
        $this->db->limit($limit);
    }

		$this->db->order_by('news_id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function updateNews($fields = false, $conditions){

        try 
        {
            $this->db->where($conditions);
            if($fields != FALSE){
                $this->db->update('news_and_events', $fields); 
                return TRUE;
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }	
  	}

  	public function deleteNews($condition = false){
        try {

           if( $condition!= FALSE ){
                $this->db->delete('news_and_events', $condition); 
                return TRUE;
           } 
           else {
                return FALSE;
           }

        } catch (Exception $e) {
            echo $e->getMessage();
        }	
  	}
}
