<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staticpages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Programmes_model');
		$this->load->model('Blogs_model');
		$this->load->model('Settings_model');
	}


	public function index()
	{
		// $headerData['title'] = "Contact us";
		$pageData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug','cover_image','description'), FALSE);
		$pageData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);
		$allBlogs = $this->Blogs_model->listBlogs(FALSE, FALSE);
		$randomBlog = rand(0, count($allBlogs)-1); 
		$pageData['blog'] = $allBlogs[$randomBlog];
		$randomProgram = rand(0, count($pageData['programmes'])-1); 
		$pageData['randomProgram'] = $pageData['programmes'][$randomProgram];

		// $this->load->view('themes/frontend-header-home', $headerData);
		$this->load->view('frontend/home', $pageData);
		// $this->load->view('themes/frontend-footer', $footerData);
	}

    /**
     * @uses 		To delete trainer details from db
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	

	public function page404()
	{
		$this->load->view('404');
	}

    /**
     * @uses 		To display list of trainers
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
	public function trainers()
	{
		$this->load->model('Trainers_model');
		$headerData['title'] = "Trainers";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$data['trainers'] = $this->Trainers_model->listTrainers(FALSE, FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/trainers',$data);
		$this->load->view('themes/frontend-footer', $footerData);
	}

    /**
     * @uses 		To display details of a trainer
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
/*	public function trainerDetails($trainerId)
	{
		if ($trainerId) {
			$this->load->model('Trainers_model');
			$data['trainerDetails'] = $this->Trainers_model->listTrainers(false, array('trainer_id' => $trainerId ) );
			$headerData['title'] = "Trainers";
			$headerData['programmes'] = $this->Programmes_model->listProgrammes( array('title','slug'), FALSE);
			$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

			$this->load->view('themes/frontend-header', $headerData);
			$this->load->view('frontend/trainerDetails',$data);
			$this->load->view('themes/frontend-footer', $footerData);
		}

	}*/

    /**
     * @uses 		To delete trainer details from db
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
	public function contactUs()
	{
		$headerData['title'] = "Contact us";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/contact', $footerData);
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
     * @uses 		To 
     * @author      Harsha 
     * @param       NIL
     * Date     :   12/07/2016
    */	
    public function submitContact(){

    	$this->load->model('Contactus_model');

    	$data = [
    		'name' =>$this->input->post('name'),
    		'email' =>$this->input->post('email'),
    		'phone' =>$this->input->post('phone'),
    		'subject' =>$this->input->post('subject'),
    		'content' =>$this->input->post('content'),

    	];

    	$this->Contactus_model->createContact($data);

    	redirect('Staticpages/contactUs');

    }


    /**
     * @uses 		To load a program 
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
    public function programmes($slug = NULL){
    	$this->load->model('Testimonials_model');

    	if($slug!= ''){

    		$programDetails = $this->Programmes_model->listProgrammes( array('title'), array('slug' => $slug ) );

   		 	//If Program Exists
    		if(!empty($programDetails)){
    			$headerData['title'] = $programDetails[0]->title;
				$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
				$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

    			$pageData['programmes'] = $this->Programmes_model->listProgrammes(FALSE, array('slug' => $slug ));
    			$test = $pageData['programmes'][0]->main_programme_id;
    			$pageData['testimonials'] = $this->Testimonials_model->listTestimonials(FALSE, array('main_programme_id' => $pageData['programmes'][0]->main_programme_id ));

/*    			print_r(json_encode($pageData['testimonials']));
    			exit;
*/
    			$this->load->view('themes/frontend-header', $headerData);
    			$this->load->view('frontend/viewProgramme', $pageData);
				$this->load->view('themes/frontend-footer', $footerData);
    		}
    		else{
    			redirect('staticpages/index');
    		}

    	} else{
    		redirect('staticpages/index');
    	}
    }

 	/**
 	 * @uses        To display details about the institute- static page
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
 	public function aboutInstitute()
	{
		$this->load->model('NewsandEvents_model');
		$headerData['title'] = "About Institute";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);
		$data['allNews'] = $this->NewsandEvents_model->listNews(FALSE, False);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/aboutInstitute', $data);
		$this->load->view('themes/frontend-footer', $footerData);
	}


 	/**
     * @author      Sharan 
     * @param       NIL
     * Date     :   31/07/2016
    */	
 	public function aboutGuruji()
	{
		$headerData['title'] = "Guruji Sanjeev";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/aboutGuruji');
		$this->load->view('themes/frontend-footer', $footerData);
	}



 	/**
     * @author      Harsha 
 	 * @uses        To display list of centres
     * @param       NIL
     * Date     :   10/06/2016
    */	
 	public function centres()
	{
		$this->load->model('Centres_model');
		$headerData['title'] = "Our Centres";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$data['centres'] = $this->Centres_model->listCentres(False, False);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);


		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/ourCentres', $data);
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
 	 * @uses        To display list of news and events
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
	public function news(){

		$this->load->model('NewsandEvents_model');
		$headerData['title'] = "Articles";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes( array('title','slug'), FALSE);
		$data['news'] = $this->NewsandEvents_model->listNews(FALSE, False);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/news', $data);
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
     * @author      Harsha 
 	 * @uses        Retrieve details of news/events from db by sending slug and display the details
     * @param       NIL
     * Date     :   10/06/2016
    */	
    public function newsDetails($slug){

		$this->load->model('NewsandEvents_model');
		if($slug!= false){
			$headerData['title'] = "Articles";
			$headerData['programmes'] = $this->Programmes_model->listProgrammes( array('title','slug'), FALSE);
			$data['newsDetails'] =  $this->NewsandEvents_model->listNews(FALSE, array('slug' => $slug));
			$data['allNews'] =  $this->NewsandEvents_model->listNews(array('title','slug'), FALSE, 10);
			$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

			$this->load->view('themes/frontend-header', $headerData);
			$this->load->view('frontend/newsDetails', $data);
			$this->load->view('themes/frontend-footer', $footerData);
		} else {
			redirect('/articles');
		}
    }

 	/**
     * @author      Harsha 
     * @param       NIL
     * Date     :   10/06/2016
    */	
/*   	public function events(){

		$headerData['title'] = "Events";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/events');
		$this->load->view('themes/frontend-footer', $footerData);
	}*/

 	/**
     * @author      Harsha 
 	 * @uses        To display list of blogs
     * @param       NIL
     * Date     :   16/06/2016
    */	
	public function blogs(){

		$headerData['title'] = "Blogs";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$data['blogs'] = $this->Blogs_model->listBlogs(FALSE, FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/blogs', $data);
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
     * @author      Harsha 
 	 * @uses        Retrieve details of a blog from db by sending slug and display the details
     * @param       NIL
     * Date     :   16/06/2016
    */	
 	public function blogDetails($slug = false){

		if($slug!= false){
			$data['blogDetails'] =  $this->Blogs_model->listBlogs(FALSE, array('slug' => $slug));
			$data['allBlogs'] =  $this->Blogs_model->listBlogs(array('title','slug','cover_image','impressions'), FALSE, 10);

			//Redirect if no Blog with the above slug
			if(!$data['blogDetails']){
				$headerData['title'] = 'Oops!';
			} else {
				$headerData['title'] = $data['blogDetails'][0]->title;
				$this->Blogs_model->incrementImpressions($data['blogDetails'][0]->blog_id);
			}

			$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
			$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

			//Increment Count

			$this->load->view('themes/frontend-header', $headerData);
			$this->load->view('frontend/blogDetails', $data);
			$this->load->view('themes/frontend-footer', $footerData);
		}
		else{
			redirect('blogs');
		}
 	}


 	/**
     * @author      Harsha 
     * @param       NIL
     * Date     :   16/06/2016
    */	
    public function stories(){

		$headerData['title'] = "Stories";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/stories');
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
     * @author      Harsha 
     * @param       NIL
     * Date     :   16/06/2016
    */	
	public function studentExperiences(){

		$headerData['title'] = "Student Experiences";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/studentExperiences');
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
     * @author      Harsha 
     * @param       NIL
     * Date     :   16/06/2016
    */	
	public function shortTestimonials(){

		$headerData['title'] = "Short Testimonials";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/shortTestimonials');
		$this->load->view('themes/frontend-footer', $footerData);
	}

 	/**
     * @author      Harsha 
     * @param       NIL
     * Date     :   12/07/2016
    */		
	public function sendMail(){
		$this->load->model('Email_model');
		$mailTemplate = $this->Email_model->listTemplates( FALSE, array('type' => 'welcome' ) );

		$to_email = 'sharan2040@gmail.com'; 

		$this->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "smtp.gmail.com";
		$config['smtp_port'] = "587";
		$config['smtp_user'] = "technical@elegantlinelogistics.com"; 
		$config['smtp_pass'] = "Mohandaskm55";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$this->email->initialize($config);

		$this->email->from('technical@elegantlinelogistics.com', 'Elegant Line Logistics');
		$this->email->to($to_email);
		$this->email->subject('Recieved your Quote Request');
		$this->email->message($mailTemplate[0]->template);
		$this->email->send();
	}

 	/**
     * @author      Harsha 
     * @param       NIL
     * Date     :   12/07/2016
    */			
 	public function submitTestimonial(){
 		$this->load->model('Testimonials_model');

 		$data = [
 			'student_name' => $this->input->post('name'),
 			'email' => $this->input->post('email'),
 			'content' => $this->input->post('story'),
 			'main_programme_id' => $this->input->post('mainProgramId'),
 		];

    	$this->Testimonials_model->createTestimonial($data);
    	redirect('staticpages/index');
 	}
// https://www.googleapis.com/youtube/v3/playlists?channelId=UCy90TiW5_vhToja1vf5NiKw&part=snippet&key=AIzaSyDXTsvR5kUlpzLkI8W_P4usZWbCX3mWsdw
 	

 	public function videoGallery(){

 		$headerData['title'] = "All Playlists";
		$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
		$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

	 	$channedID = 'UCy90TiW5_vhToja1vf5NiKw'; // The Channel ID
	    $apiKey = 'AIzaSyDXTsvR5kUlpzLkI8W_P4usZWbCX3mWsdw';
	    $playlists = 'https://www.googleapis.com/youtube/v3/playlists?key='.$apiKey.'&part=snippet&maxResults=10&channelId=' . $channedID;
	    $json = file_get_contents($playlists);
	    $json = preg_replace('/^\xEF\xBB\xBF/', '', $json);
	    $array = json_decode($json, TRUE);

	    $data['array'] = $array;

		$this->load->view('themes/frontend-header', $headerData);
		$this->load->view('frontend/youtubePlaylists', $data);
		$this->load->view('themes/frontend-footer', $footerData);
 	}

 	public function playlistVideos(){

 		$playlistId = $this->input->post('playlistId', true);
 		$playlistTitle = $this->input->post('playlistTitle', true);

 		if($playlistId!= '' && $playlistTitle!=''){
	 		$headerData['title'] = $playlistTitle;
			$headerData['programmes'] = $this->Programmes_model->listProgrammes(  array('title','slug'), FALSE);
			$footerData['settings'] = $this->Settings_model->getSettings(FALSE, FALSE);

		 	// $playlistID = 'PLz2YY2kN0e_p4nUs_6WiIZyAvLk76RH57'; // The Playlist ID for your 'uploads'-playlist.
		    $apiKey = 'AIzaSyDXTsvR5kUlpzLkI8W_P4usZWbCX3mWsdw';
		    $videofile = 'https://www.googleapis.com/youtube/v3/playlistItems?key='.$apiKey.'&part=snippet&maxResults=10&playlistId=' . $playlistId;
		    $json = file_get_contents($videofile);
		    $json = preg_replace('/^\xEF\xBB\xBF/', '', $json);
		    $array = json_decode($json, TRUE);

		    $data['array'] = $array;

			$this->load->view('themes/frontend-header', $headerData);
			$this->load->view('frontend/videoGallery', $data);
			$this->load->view('themes/frontend-footer', $footerData);
		} else {
			redirect('/');
		}
 	}



} // Controller Ends Here
