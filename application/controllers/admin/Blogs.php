<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Blogs extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

        $this->load->model('Blogs_model');
        $this->load->model('Reminders_model');
    }

    /**
     * @uses 		To retrieve list of Blogs 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
    public function index(){

        $data['blogs'] = $this->Blogs_model->listBlogs(false, false);
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
        $this->load->view('/themes/admin-header', $headerData);
        $this->load->view('admin/blogs/list', $data);
        $this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To load view to add new Blog 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */  
    public function add(){

        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
        $this->load->view('/themes/admin-header', $headerData);
        $this->load->view('admin/blogs/add');
        // $this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To create a new Blog 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */  
    public function create(){

         //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/blogs/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('coverImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();
        }


        //Image Upload Ends
        $data = [
        'title' => $this->input->post('title'),
        'slug' => url_title( $this->input->post('title') , 'dash', true),
        'contents' => $this->input->post('content'),
        ];

        if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

        $this->Blogs_model->createBlog($data);

        //For Displaying message in the next page
        $this->session->set_flashdata('blogCreated', true);

        redirect('admin/blogs');
    }

    /**
     * @uses        To load view to edit a Blog 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */  
    public function edit($blogId){

        $data['blogs'] = $this->Blogs_model->listBlogs(false, array('blog_id' => $blogId ) );
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
        $this->load->view('/themes/admin-header', $headerData);
        $this->load->view('admin/blogs/edit', $data);
        // $this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To update a Blog 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */
    public function update(){
        
        $uploadedImage = '';
        $blogId = $this->input->post('blogId');

             //Image Upload Starts
            $config['upload_path'] = FCPATH . 'assets/uploads/blogs/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['file_name'] = md5(uniqid(rand(), true));
            $config['overwrite'] = false;

            // print_r($config['upload_path']);
            $this->load->library('upload', $config);

        if( is_uploaded_file($_FILES['coverImage']['tmp_name']) ){

            if ( ! $this->upload->do_upload('coverImage'))
            {
                $error = array('error' => $this->upload->display_errors());
                print_r($error); break;
                $uploadedImage = '';
            }
            else
            {
                $uploadedImage = $this->upload->data();
                $oldCoverImage = $this->Blogs_model->listBlogs( array('cover_image'), array('blog_id' => $blogId  ) );
                unlink(FCPATH."assets/uploads/blogs/".$oldCoverImage[0]->cover_image); //Delete a old file if new one is added                
            }
                //Image Upload Ends
        }


        $data = [
            'title' => $this->input->post('title'),
            'slug' => url_title( $this->input->post('title') , 'dash', true),
            'contents' => $this->input->post('content'),
        ];

        if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

        $this->Blogs_model->updateBlog($data, array('blog_id' => $blogId ) );

        //For Displaying message in the next page
        $this->session->set_flashdata('blogUpdated', true);

        redirect('admin/blogs');   
    }

    /**
     * @uses        To Delete a Blog 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */
    public function delete(){

        $blogId = $this->input->post('blogId');
        if($blogId){
            //Delete file from folder
            $oldCoverImage = $this->Blogs_model->listBlogs( array('cover_image'), array('blog_id' => $blogId  ) );
            unlink(FCPATH."assets/uploads/blogs/".$oldCoverImage[0]->cover_image);
            
            $this->Blogs_model->deleteBlog( array('blog_id' => $blogId ) );

            //For Displaying message in the next page
            $this->session->set_flashdata('blogDeleted', true);

            redirect('admin/blogs');
        }
        else{
            redirect('admin/blogs');
        }
    }

}//controller ends here