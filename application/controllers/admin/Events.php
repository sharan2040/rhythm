<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Events extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

        $this->load->model('Events_model');
        $this->load->model('Reminders_model');
	}

    /**
     * @uses 		To retrieve list of Events 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
    public function index(){

        $data['events'] = $this->Events_model->listEvents(false, false);
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

    	$this->load->view('/themes/admin-header', $headerData);
        $this->load->view('admin/events/list', $data);
    	$this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To load view to add new Event 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */  
    public function add(){

        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

        $this->load->view('/themes/admin-header', $headerData);
        $this->load->view('admin/events/add');
        $this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To create a new Event 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */  
    public function create(){

         //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('coverImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();
        }


        //Image Upload Ends
        $data = [
        'title' => $this->input->post('title'),
        'slug' => url_title( $this->input->post('title') , 'dash', true),
        'content' => $this->input->post('content'),
        ];

        if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

        $this->Events_model->createEvent($data);

        //For Displaying message in the next page
        $this->session->set_flashdata('eventCreated', true);

        redirect('admin/events');
    }

    /**
     * @uses        To load view to edit a Event 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */  
    public function edit($eventId){

        $data['events'] = $this->Events_model->listEvents(false, array('event_id' => $eventId ) );
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
        $this->load->view('/themes/admin-header', $headerData);
        $this->load->view('admin/events/edit', $data);
        $this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To update a Event 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */
    public function update(){
        $eventId = $this->input->post('eventId');

         //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('coverImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();
        }


        //Image Upload Ends
        $data = [
        'title' => $this->input->post('title'),
        'slug' => url_title( $this->input->post('title') , 'dash', true),
        'content' => $this->input->post('content'),
        ];

        if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

        $this->Events_model->updateEvent($data, array('event_id' => $eventId ) );

        //For Displaying message in the next page
        $this->session->set_flashdata('eventUpdated', true);

        redirect('admin/events');   
    }

    /**
     * @uses        To Delete a Event 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   13/06/2016
    */
    public function delete(){

        $eventId = $this->input->post('eventId');
        if($eventId){
            $this->Events_model->deleteEvent( array('event_id' => $eventId ) );

            //For Displaying message in the next page
            $this->session->set_flashdata('eventDeleted', true);

            redirect('admin/events');
        }
        else{
            redirect('admin/events');
        }
    }

}//controller ends here