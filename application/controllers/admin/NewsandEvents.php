<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class NewsandEvents extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

        $this->load->model('NewsandEvents_model');
        $this->load->model('Reminders_model');
	}

    /**
     * @uses 		To retrieve list of centres 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   17/06/2016
    */	
    public function index(){

        $data['news'] = $this->NewsandEvents_model->listNews(FALSE, FALSE);
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') )); 

    	$this->load->view('/themes/admin-header');
        $this->load->view('admin/newsandevents/list', $data);
    	$this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To load create view   
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   19/06/2016
    */  
    public function add(){

        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

        $this->load->view('/themes/admin-header');
        $this->load->view('admin/newsandevents/add');
        $this->load->view('/themes/admin-footer');
    }

    /**
     * @uses        To create a new article   
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   19/06/2016
    */  
    public function create(){

         //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/news_and_events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('coverImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();
        }


        //Image Upload Ends
        $data = [
        'title' => $this->input->post('title'),
        'slug' => url_title( $this->input->post('title') , 'dash', true),
        'contents' => $this->input->post('content'),
        ];

        if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

        $this->NewsandEvents_model->createNews($data);

        //For Displaying message in the next page
        $this->session->set_flashdata('newsCreated', true);

        redirect('admin/NewsandEvents');
    }

    /**
     * @uses        To load  wdit view and retrieve data from db  
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   19/06/2016
    */  
    public function edit($newsId){

        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
        $data['news'] = $this->NewsandEvents_model->listNews(FALSE, array('news_Id' => $newsId ) );

        $this->load->view('/themes/admin-header');
        $this->load->view('admin/newsandevents/edit', $data);
        $this->load->view('/themes/admin-footer');   
    }

    /**
     * @uses        To load  wdit view and retrieve data from db  
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   19/06/2016
    */  
    public function update(){
        $newsId = $this->input->post('newsId');

         //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/news_and_events/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);

        if( is_uploaded_file($_FILES['coverImage']['tmp_name']) ){


            if ( ! $this->upload->do_upload('coverImage'))
            {
                $error = array('error' => $this->upload->display_errors());
                print_r($error); break;
                $uploadedImage = '';
            }
            else
            {
                $uploadedImage = $this->upload->data();
                $oldCoverImage = $this->NewsandEvents_model->listNews( array('cover_image'), array('news_id' => $newsId  ) );
                unlink(FCPATH."assets/uploads/news_and_events/".$oldCoverImage[0]->cover_image); //Delete a old file if new one is added            
            }
        }

        //Image Upload Ends
        $data = [
        'title' => $this->input->post('title'),
        'slug' => url_title( $this->input->post('title') , 'dash', true),
        'contents' => $this->input->post('content'),
        ];

        if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

        $this->NewsandEvents_model->updateNews($data, array('news_id' => $newsId ) );

        //For Displaying message in the next page
        $this->session->set_flashdata('newsUpdated', true);

        redirect('admin/NewsandEvents');   
    }

    /**
     * @uses        To load  wdit view and retrieve data from db  
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   20/06/2016
    */  
    public function delete(){

        $newsId = $this->input->post('newsId');

        if($newsId){

            //Delete Image from Folder
            $oldCoverImage = $this->NewsandEvents_model->listNews( array('cover_image'), array('news_id' => $newsId  ) );
            unlink(FCPATH."assets/uploads/news_and_events/".$oldCoverImage[0]->cover_image);

            //Detele from DB
            $this->NewsandEvents_model->deleteNews( array('news_id' => $newsId ) );

            //For Displaying message in the next page
            $this->session->set_flashdata('newsDeleted', true);

            redirect('admin/NewsandEvents');
        }
        else{
            redirect('admin/NewsandEvents');
        }   
    }


}// Controller ends here