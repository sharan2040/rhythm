<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
/**
 * @uses    : To Manage Programs
 * @author  : Harsha
 * Date     : 01/06/2016
 * Type     : Controller
*/

class Experiences extends Admin_Controller {
	
    public function __construct(){
		parent::__construct();

		$this->load->model('Programmes_model');
        $this->load->model('Reminders_model');
	}

    /**
     * @uses 		To list all programmes
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function index(){
		
		$data['programmes'] = $this->Programmes_model->listProgrammes(false, false);
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/programmes/list', $data);
		$this->load->view('/themes/admin-footer');	
	
	}

    /**
     * @uses 		To load form for creating a Programmes
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
   	public function add(){
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') )); 

		$this->load->view('/themes/admin-header',$headerData);
		$this->load->view('admin/programmes/add');
		$this->load->view('/themes/admin-footer');	
	}

    /**
     * @uses 		To create a new Programme 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function create(){

	 //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/programs/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1500';
        $config['max_width']  = '1600';
        $config['max_height']  = '1200';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('coverImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();
        }


     //Image Upload Ends	

		$data = [
            'title' => $this->input->post('title'),
            'main_caption' => $this->input->post('mainCaption'),
            'sub_caption' => $this->input->post('subCaption'),
			'slug' => url_title($this->input->post('title'), 'dash', true),
			'description' => $this->input->post('description')
		];

		if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

		$this->Programmes_model->createProgramme($data);

		//For Displaying message in the next page
		$this->session->set_flashdata('programmeCreated', true);

		redirect('admin/programmes');
	}

    /**
     * @uses 		To retrieve details of a Programme from db
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function edit($mainProgrammeId){

		$data['programmes'] = $this->Programmes_model->listProgrammes(false, array('main_programme_id' => $mainProgrammeId ) );
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/programmes/edit', $data);
		$this->load->view('/themes/admin-footer');	
	}

    /**
     * @uses 		To update the details of a Programme 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function update(){

		//Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/programs/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1500';
        $config['max_width']  = '1600';
        $config['max_height']  = '1200';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('coverImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();
        }


        //Image Upload Ends			
		$mainProgrammeId = $this->input->post('mainProgrammeId', true);

        $data = [
            'title' => $this->input->post('title'),
            'main_caption' => $this->input->post('mainCaption'),
            'sub_caption' => $this->input->post('subCaption'),
            'slug' => url_title($this->input->post('title'), 'dash', true),
            'description' => $this->input->post('description')
        ];

		if(isset($uploadedImage['file_name'])){
            $data['cover_image'] = $uploadedImage['file_name'];
        }

		$this->Programmes_model->updateProgramme($data, array('main_programme_id' => $mainProgrammeId ) );

		//For Displaying message in the next page
		$this->session->set_flashdata('programmeUpdated', true);

		redirect('admin/programmes');

	}

    /**
     * @uses 		To delete a Programme 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function delete(){
		$mainProgrammeId = $this->input->post('mainProgrammeId', true);
		
		if($mainProgrammeId){
    		$this->Programmes_model->deleteProgramme( array('main_programme_id' => $mainProgrammeId ) );

    		//For Displaying message in the next page
    		$this->session->set_flashdata('programmeDeleted', true);

    		redirect('admin/programmes');
		}
        else{

            redirect('admin/programmes');
        }
	}

}// Controller ends here