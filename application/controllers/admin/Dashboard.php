<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('Reminders_model');
	}
	
	public function index()
	{
	
		// $data['new_enquiries_count'] = count($this->Enquiry_model->listEnquiries( array('enquiry_id'), array('status' => '0' )));
		// $data['attended_enquiries_count'] = count($this->Enquiry_model->listEnquiries( array('enquiry_id'), array('status' => '1' )));
		// $data['rejected_enquiries_count'] = count($this->Enquiry_model->listEnquiries( array('enquiry_id'), array('status' => '2' )));
			// print_r($data); break;
		$headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
		$this->load->view('/themes/admin-header',$headerData);
		$this->load->view('admin/dashboard');
		$this->load->view('/themes/admin-footer');	
	}

	

} // Controller Ends Here
