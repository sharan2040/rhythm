<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @uses    : To Manage Centres
 * @author  : Harsha
 * Date     : 10/06/2015
 * Type     : Controller
*/

class Centres extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Centres_model');
        $this->load->model('Reminders_model');
	}

    /**
     * @uses 		To retrieve list of centres 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function index(){
		$data['centres'] = $this->Centres_model->listCentres(false, false);
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/centres/list', $data);
		$this->load->view('themes/admin-footer');

	}

    /**
     * @uses 		To load create form 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
    public function add(){
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/centres/add');
		$this->load->view('themes/admin-footer');
    }

    /**
     * @uses 		To create a new centre 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
    public function create(){
    	$data = [
    		'centre_name' => $this->input->post('centreName'),
    		'centre_address' => $this->input->post('centreAddress'),
    		'contact_number' => $this->input->post('contactNumber'),
    		'latitude' => $this->input->post('latitude'),
    		'longitude' => $this->input->post('longitude'),
    	];

    	$this->Centres_model->createCentre($data);
    	redirect('admin/centres');
    }	

    /**
     * @uses 		To edit a selected centre 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
	public function edit($centreId){

		$data['centres'] = $this->Centres_model->listCentres(false, array('centre_id' => $centreId ) );
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));
		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/centres/edit', $data);
		$this->load->view('themes/admin-footer');

	}

    /**
     * @uses 		To update details of a centre  
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   11/06/2016
    */	
    public function update(){

    	$centreId = $this->input->post('centreId');
		$data = [
			'centre_name' => $this->input->post('centreName'),
			'centre_address' => $this->input->post('centreAddress'),
			'contact_number' => $this->input->post('contactNumber'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
		];   

		$this->Centres_model->updateCentre($data, array('centre_id' =>$centreId ) );
		redirect('admin/centres');
	} 

    /**
     * @uses 		To delete centre details from db
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */	
	public function delete(){

    	$centreId = $this->input->post('centreId');
    	if($centreId){
    		$this->Centres_model->deleteCentre( array('centre_id' =>$centreId ) );

   			redirect('admin/centres');
   		}
        else{
            redirect('admin/centres');
        }
	}

}//controller ends here
