<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Trainers extends Admin_Controller
{
	
    public function __construct()
	{
		parent::__construct();

		$this->load->model('Trainers_model');
        $this->load->model('Reminders_model');
	}

    /**
     * @uses 		To retrieve list of trainers 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */	
   	public function index(){
		$data['trainers'] = $this->Trainers_model->listTrainers(false, false);
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/trainers/list', $data);
		$this->load->view('/themes/admin-footer');
	}

    /**
     * @uses To load form for adding trainer
     * @author      Harsha
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */
	public function add(){
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/trainers/add');
		$this->load->view('/themes/admin-footer');
	}

    /**
     * @uses 		To add details of trainer into database
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */		
    public function create(){
	 //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/trainers/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if ( ! $this->upload->do_upload('profileImage'))
        {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); break;
            $uploadedImage = '';
        }
        else
        {
            $uploadedImage = $this->upload->data();

        }


     //Image Upload Ends			
		$data = [
			'trainer_name' => $this->input->post('trainerName'),
			'bio' => $this->input->post('bio'),
			'facebook_url' => $this->input->post('facebookUrl'),
			'twitter_url' => $this->input->post('twitterUrl'),
			'instagram_url' => $this->input->post('instagramUrl'),
			'mail_address' => $this->input->post('mailAddress'),
			'mobile_number' => $this->input->post('mobileNumber'),
		];

		if(isset($uploadedImage['file_name'])){
            $data['profile_image'] = $uploadedImage['file_name'];
        }

		$this->Trainers_model->createTrainer($data);
		redirect('admin/trainers');
	}

    /**
     * @uses 		To retrieve details of selected trainer from db
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */	
	public function edit($trainerId){
		$data['trainers'] = $this->Trainers_model->listTrainers(false, array('trainer_id' => $trainerId ) );
        $headerData['reminders'] = $this->Reminders_model->listReminder(false , array('reminder_date' => date('Y-m-d') ));

		$this->load->view('/themes/admin-header', $headerData);
		$this->load->view('admin/trainers/edit', $data);
		$this->load->view('/themes/admin-footer');
	}
 
    /**
     * @uses 		To update details of trainer into db
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */	
   	public function update(){

        $trainerId = $this->input->post('trainerId');

	 //Image Upload Starts
        $config['upload_path'] = FCPATH . 'assets/uploads/trainers';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $config['overwrite'] = false;

        // print_r($config['upload_path']);
        $this->load->library('upload', $config);


        if( is_uploaded_file($_FILES['profileImage']['tmp_name']) ){

            if ( ! $this->upload->do_upload('profileImage'))
            {
                $error = array('error' => $this->upload->display_errors());
                print_r($error); break;
                $uploadedImage = '';
            }
            else
            {
                $uploadedImage = $this->upload->data();
                $oldCoverImage = $this->Trainers_model->listTrainers( array('profile_image'), array('trainer_id' => $trainerId  ) );
                unlink(FCPATH."assets/uploads/trainers/".$oldCoverImage[0]->profile_image); //Delete a old file if new one is added    
            }

        }


     //Image Upload Ends			
		$data = [
			'trainer_name' => $this->input->post('trainerName'),
			'bio' => $this->input->post('bio'),
			'facebook_url' => $this->input->post('facebookUrl'),
			'twitter_url' => $this->input->post('twitterUrl'),
			'instagram_url' => $this->input->post('instagramUrl'),
			'mail_address' => $this->input->post('mailAddress'),
			'mobile_number' => $this->input->post('mobileNumber'),
		];

		if(isset($uploadedImage['file_name'])){
            $data['profile_image'] = $uploadedImage['file_name'];
        }

		$this->Trainers_model->updateTrainer($data, array('trainer_id' => $trainerId ) );
		redirect('admin/trainers');	
	}

    /**
     * @uses 		To delete trainer details from db
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   10/06/2016
    */	
   	public function delete(){
		$trainerId = $this->input->post('trainerId', true);
		
		if($trainerId){

            //Delete file from folder
            $oldCoverImage = $this->Trainers_model->listTrainers( array('profile_image'), array('trainer_id' => $trainerId  ) );
            unlink(FCPATH."assets/uploads/trainers/".$oldCoverImage[0]->profile_image); 
                       
    		$this->Trainers_model->deleteTrainer( array('trainer_id' => $trainerId ) );

    		//For Displaying message in the next page
    		$this->session->set_flashdata('trainerDeleted', true);

    		redirect('admin/trainers');
		}
        else{
            redirect('admin/trainers');
        }
	}
}// Controller ends here

