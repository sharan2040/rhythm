<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 

*/
class Testimonials extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Testimonials_model');
		$this->load->model('Programmes_model');
	}


    /**
     * @uses 		To retrieve list of testimonials 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */	
    public function index(){

    	$data['testimonials'] = $this->Testimonials_model->listing();
/*        print_r(json_encode($data['testimonials']));
        exit;
*//*    	$data['programmes'] = $this->Programmes_model->listProgrammes(array('main_programme_id', 'title'), FALSE);
*/
		$this->load->view('/themes/admin-header');
		$this->load->view('admin/testimonials/list', $data);
		$this->load->view('/themes/admin-footer');
    }

    /**
     * @uses 		Load view to add a new testimonial 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */	
   	public function add(){

		$data['programmes'] = $this->Programmes_model->listProgrammes(array('main_programme_id', 'title'), FALSE);
		$this->load->view('/themes/admin-header');
		$this->load->view('admin/testimonials/add', $data);
		$this->load->view('/themes/admin-footer');
	}

    /**
     * @uses 		To create a new testimonial 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */	
    public function create(){
    	$data = [
    		'student_name' => $this->input->post('studentName'),
    		'content' => $this->input->post('content'),
    		'main_programme_id' =>$this->input->post('mainProgramId')
    	];
    	$this->Testimonials_model->createTestimonial($data);

        //For Displaying message in the next page
        $this->session->set_flashdata('testimonialCreated', true);
    	redirect('admin/testimonials');
    }

    /**
     * @uses 		Retrieve data from db to edit a testimonial 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */	
    public function edit($testimonialId){

		$data['testimonials'] = $this->Testimonials_model->listTestimonials(FALSE, array('testimonial_id' => $testimonialId ) );
		$data['programmes'] = $this->Programmes_model->listProgrammes(array('main_programme_id', 'title'), FALSE);

		$this->load->view('/themes/admin-header');
		$this->load->view('admin/testimonials/edit', $data);
		$this->load->view('/themes/admin-footer');
    }

    /**
     * @uses 		Retrieve data from db to edit a testimonial 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */	
    public function update(){

    	$testimonialId = $this->input->post('testimonialId');

    	$data = [
    		'student_name' => $this->input->post('studentName'),
    		'content' => $this->input->post('content'),
            'status' =>$this->input->post('currentStatus'),
    		'main_programme_id' =>$this->input->post('mainProgramId')
    	];

		$this->Testimonials_model->updateTestimonial($data, array('testimonial_id' =>$testimonialId ) );

        //For Displaying message in the next page
        $this->session->set_flashdata('testimonialUpdated', true);
    	redirect('admin/testimonials');
    }

    /**
     * @uses 		Retrieve data from db to edit a testimonial 
     * @author      Harsha 
     * @param       NIL
     * Method   :   POST
     * Date     :   27/06/2016
    */	

    public function delete(){
    	$testimonialId = $this->input->post('testimonialId');

    	if($testimonialId){
	    	$this->Testimonials_model->deleteTestimonial(array('testimonial_id' => $testimonialId ) );

        //For Displaying message in the next page
        $this->session->set_flashdata('testimonialDeleted', true);

	    	redirect('admin/testimonials');
	    }
	    else{
	    	redirect('admin/testimonials');
	    }

    }
}



